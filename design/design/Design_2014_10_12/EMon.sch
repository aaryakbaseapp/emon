<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="120" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BaseApp">
<packages>
<package name="QFN32">
<circle x="-2.175" y="2.175" radius="0.15" width="0" layer="21"/>
<wire x1="-2.45" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="2.05" x2="-2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="2.45" x2="-2.05" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.05" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="2.05" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.05" x2="2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="2.05" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="-2.05" width="0.1016" layer="21"/>
<smd name="1" x="-2.4" y="1.75" dx="0.7112" dy="0.25" layer="1"/>
<smd name="2" x="-2.4" y="1.25" dx="0.7112" dy="0.25" layer="1"/>
<smd name="3" x="-2.4" y="0.75" dx="0.7112" dy="0.25" layer="1"/>
<smd name="4" x="-2.4" y="0.25" dx="0.7112" dy="0.25" layer="1"/>
<smd name="5" x="-2.4" y="-0.25" dx="0.7112" dy="0.25" layer="1"/>
<smd name="6" x="-2.4" y="-0.75" dx="0.7112" dy="0.25" layer="1"/>
<smd name="7" x="-2.4" y="-1.25" dx="0.7112" dy="0.25" layer="1"/>
<smd name="8" x="-2.4" y="-1.75" dx="0.7112" dy="0.25" layer="1"/>
<smd name="9" x="-1.75" y="-2.4" dx="0.7112" dy="0.25" layer="1" rot="R90"/>
<smd name="10" x="-1.25" y="-2.4" dx="0.7112" dy="0.25" layer="1" rot="R90"/>
<smd name="11" x="-0.75" y="-2.4" dx="0.7112" dy="0.25" layer="1" rot="R90"/>
<smd name="12" x="-0.25" y="-2.4" dx="0.7112" dy="0.25" layer="1" rot="R90"/>
<smd name="13" x="0.25" y="-2.4" dx="0.7112" dy="0.25" layer="1" rot="R90"/>
<smd name="14" x="0.75" y="-2.4" dx="0.7112" dy="0.25" layer="1" rot="R90"/>
<smd name="15" x="1.25" y="-2.4" dx="0.7112" dy="0.25" layer="1" rot="R90"/>
<smd name="16" x="1.75" y="-2.4" dx="0.7112" dy="0.25" layer="1" rot="R90"/>
<smd name="17" x="2.4" y="-1.75" dx="0.7112" dy="0.25" layer="1" rot="R180"/>
<smd name="18" x="2.4" y="-1.25" dx="0.7112" dy="0.25" layer="1" rot="R180"/>
<smd name="19" x="2.4" y="-0.75" dx="0.7112" dy="0.25" layer="1" rot="R180"/>
<smd name="20" x="2.4" y="-0.25" dx="0.7112" dy="0.25" layer="1" rot="R180"/>
<smd name="21" x="2.4" y="0.25" dx="0.7112" dy="0.25" layer="1" rot="R180"/>
<smd name="22" x="2.4" y="0.75" dx="0.7112" dy="0.25" layer="1" rot="R180"/>
<smd name="23" x="2.4" y="1.25" dx="0.7112" dy="0.25" layer="1" rot="R180"/>
<smd name="24" x="2.4" y="1.75" dx="0.7112" dy="0.25" layer="1" rot="R180"/>
<smd name="25" x="1.75" y="2.4" dx="0.7112" dy="0.25" layer="1" rot="R270"/>
<smd name="26" x="1.25" y="2.4" dx="0.7112" dy="0.25" layer="1" rot="R270"/>
<smd name="27" x="0.75" y="2.4" dx="0.7112" dy="0.25" layer="1" rot="R270"/>
<smd name="28" x="0.25" y="2.4" dx="0.7112" dy="0.25" layer="1" rot="R270"/>
<smd name="29" x="-0.25" y="2.4" dx="0.7112" dy="0.25" layer="1" rot="R270"/>
<smd name="30" x="-0.75" y="2.4" dx="0.7112" dy="0.25" layer="1" rot="R270"/>
<smd name="31" x="-1.25" y="2.4" dx="0.7112" dy="0.25" layer="1" rot="R270"/>
<smd name="32" x="-1.75" y="2.4" dx="0.7112" dy="0.25" layer="1" rot="R270"/>
<smd name="EXP" x="0" y="0" dx="3" dy="3" layer="1"/>
<text x="-4.05" y="-4.35" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.8" y="3.25" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="HLK-PM01">
<pad name="N" x="-14.7" y="-2.5" drill="2"/>
<pad name="L" x="-14.7" y="2.5" drill="2"/>
<pad name="-VO" x="14.7" y="7.7" drill="2"/>
<pad name="+VO" x="14.7" y="-7.7" drill="2"/>
<wire x1="-17" y1="10.1" x2="17" y2="10.1" width="0.127" layer="21"/>
<wire x1="17" y1="10.1" x2="17" y2="-10.1" width="0.127" layer="21"/>
<wire x1="17" y1="-10.1" x2="-17" y2="-10.1" width="0.127" layer="21"/>
<wire x1="-17" y1="-10.1" x2="-17" y2="10.1" width="0.127" layer="21"/>
<text x="-1" y="7" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TQFP-32">
<wire x1="3.505" y1="3.505" x2="3.505" y2="-3.505" width="0.2032" layer="21"/>
<wire x1="3.505" y1="-3.505" x2="-3.505" y2="-3.505" width="0.2032" layer="21"/>
<wire x1="-3.505" y1="-3.505" x2="-3.505" y2="3.15" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="3.505" y2="3.505" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="-3.505" y2="3.15" width="0.2032" layer="21"/>
<circle x="-3.6576" y="3.683" radius="0.1524" width="0.2032" layer="21"/>
<smd name="1" x="-4.2926" y="2.8" dx="1.27" dy="0.3" layer="1"/>
<smd name="2" x="-4.2926" y="2" dx="1.27" dy="0.3" layer="1"/>
<smd name="3" x="-4.2926" y="1.2" dx="1.27" dy="0.3" layer="1"/>
<smd name="4" x="-4.2926" y="0.4" dx="1.27" dy="0.3" layer="1"/>
<smd name="5" x="-4.2926" y="-0.4" dx="1.27" dy="0.3" layer="1"/>
<smd name="6" x="-4.2926" y="-1.2" dx="1.27" dy="0.3" layer="1"/>
<smd name="7" x="-4.2926" y="-2" dx="1.27" dy="0.3" layer="1"/>
<smd name="8" x="-4.2926" y="-2.8" dx="1.27" dy="0.3" layer="1"/>
<smd name="9" x="-2.8" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="10" x="-2" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="11" x="-1.2" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="12" x="-0.4" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="13" x="0.4" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="14" x="1.2" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="15" x="2" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="16" x="2.8" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="17" x="4.2926" y="-2.8" dx="1.27" dy="0.3" layer="1"/>
<smd name="18" x="4.2926" y="-2" dx="1.27" dy="0.3" layer="1"/>
<smd name="19" x="4.2926" y="-1.2" dx="1.27" dy="0.3" layer="1"/>
<smd name="20" x="4.2926" y="-0.4" dx="1.27" dy="0.3" layer="1"/>
<smd name="21" x="4.2926" y="0.4" dx="1.27" dy="0.3" layer="1"/>
<smd name="22" x="4.2926" y="1.2" dx="1.27" dy="0.3" layer="1"/>
<smd name="23" x="4.2926" y="2" dx="1.27" dy="0.3" layer="1"/>
<smd name="24" x="4.2926" y="2.8" dx="1.27" dy="0.3" layer="1"/>
<smd name="25" x="2.8" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="26" x="2" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="27" x="1.2" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="28" x="0.4" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="29" x="-0.4" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="30" x="-1.2" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="31" x="-2" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="32" x="-2.8" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<text x="-3.175" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5466" y1="2.5714" x2="-3.556" y2="3.0286" layer="51"/>
<rectangle x1="-4.5466" y1="1.7714" x2="-3.556" y2="2.2286" layer="51"/>
<rectangle x1="-4.5466" y1="0.9714" x2="-3.556" y2="1.4286" layer="51"/>
<rectangle x1="-4.5466" y1="0.1714" x2="-3.556" y2="0.6286" layer="51"/>
<rectangle x1="-4.5466" y1="-0.6286" x2="-3.556" y2="-0.1714" layer="51"/>
<rectangle x1="-4.5466" y1="-1.4286" x2="-3.556" y2="-0.9714" layer="51"/>
<rectangle x1="-4.5466" y1="-2.2286" x2="-3.556" y2="-1.7714" layer="51"/>
<rectangle x1="-4.5466" y1="-3.0286" x2="-3.556" y2="-2.5714" layer="51"/>
<rectangle x1="-3.0286" y1="-4.5466" x2="-2.5714" y2="-3.556" layer="51"/>
<rectangle x1="-2.2286" y1="-4.5466" x2="-1.7714" y2="-3.556" layer="51"/>
<rectangle x1="-1.4286" y1="-4.5466" x2="-0.9714" y2="-3.556" layer="51"/>
<rectangle x1="-0.6286" y1="-4.5466" x2="-0.1714" y2="-3.556" layer="51"/>
<rectangle x1="0.1714" y1="-4.5466" x2="0.6286" y2="-3.556" layer="51"/>
<rectangle x1="0.9714" y1="-4.5466" x2="1.4286" y2="-3.556" layer="51"/>
<rectangle x1="1.7714" y1="-4.5466" x2="2.2286" y2="-3.556" layer="51"/>
<rectangle x1="2.5714" y1="-4.5466" x2="3.0286" y2="-3.556" layer="51"/>
<rectangle x1="3.556" y1="-3.0286" x2="4.5466" y2="-2.5714" layer="51"/>
<rectangle x1="3.556" y1="-2.2286" x2="4.5466" y2="-1.7714" layer="51"/>
<rectangle x1="3.556" y1="-1.4286" x2="4.5466" y2="-0.9714" layer="51"/>
<rectangle x1="3.556" y1="-0.6286" x2="4.5466" y2="-0.1714" layer="51"/>
<rectangle x1="3.556" y1="0.1714" x2="4.5466" y2="0.6286" layer="51"/>
<rectangle x1="3.556" y1="0.9714" x2="4.5466" y2="1.4286" layer="51"/>
<rectangle x1="3.556" y1="1.7714" x2="4.5466" y2="2.2286" layer="51"/>
<rectangle x1="3.556" y1="2.5714" x2="4.5466" y2="3.0286" layer="51"/>
<rectangle x1="2.5714" y1="3.556" x2="3.0286" y2="4.5466" layer="51"/>
<rectangle x1="1.7714" y1="3.556" x2="2.2286" y2="4.5466" layer="51"/>
<rectangle x1="0.9714" y1="3.556" x2="1.4286" y2="4.5466" layer="51"/>
<rectangle x1="0.1714" y1="3.556" x2="0.6286" y2="4.5466" layer="51"/>
<rectangle x1="-0.6286" y1="3.556" x2="-0.1714" y2="4.5466" layer="51"/>
<rectangle x1="-1.4286" y1="3.556" x2="-0.9714" y2="4.5466" layer="51"/>
<rectangle x1="-2.2286" y1="3.556" x2="-1.7714" y2="4.5466" layer="51"/>
<rectangle x1="-3.0286" y1="3.556" x2="-2.5714" y2="4.5466" layer="51"/>
</package>
<package name="CPOL_B">
<wire x1="-2.1" y1="2.1" x2="1" y2="2.1" width="0.1016" layer="51"/>
<wire x1="1" y1="2.1" x2="2.1" y2="1" width="0.1016" layer="51"/>
<wire x1="2.1" y1="1" x2="2.1" y2="-1" width="0.1016" layer="51"/>
<wire x1="2.1" y1="-1" x2="1" y2="-2.1" width="0.1016" layer="51"/>
<wire x1="1" y1="-2.1" x2="-2.1" y2="-2.1" width="0.1016" layer="51"/>
<wire x1="-2.1" y1="-2.1" x2="-2.1" y2="2.1" width="0.1016" layer="51"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0.1016" layer="21" curve="-128.186984"/>
<wire x1="-1.75" y1="-0.85" x2="1.75" y2="-0.85" width="0.1016" layer="21" curve="128.186984"/>
<wire x1="-2.1" y1="0.85" x2="-2.1" y2="2.1" width="0.1016" layer="21"/>
<wire x1="-2.1" y1="2.1" x2="1" y2="2.1" width="0.1016" layer="21"/>
<wire x1="1" y1="2.1" x2="2.1" y2="1" width="0.1016" layer="21"/>
<wire x1="2.1" y1="-1" x2="1" y2="-2.1" width="0.1016" layer="21"/>
<wire x1="1" y1="-2.1" x2="-2.1" y2="-2.1" width="0.1016" layer="21"/>
<wire x1="-2.1" y1="-2.1" x2="-2.1" y2="-0.85" width="0.1016" layer="21"/>
<wire x1="-1.2" y1="1.5" x2="-1.2" y2="-1.5" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="1.95" width="0.1016" layer="51"/>
<smd name="-" x="-1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<smd name="+" x="1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<text x="-2.15" y="2.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.15" y="-3.275" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3" y1="-0.35" x2="-1.85" y2="0.35" layer="51"/>
<rectangle x1="1.9" y1="-0.35" x2="2.3" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-1.25" y="1.45"/>
<vertex x="-1.7" y="0.85"/>
<vertex x="-1.85" y="0.35"/>
<vertex x="-1.85" y="-0.4"/>
<vertex x="-1.7" y="-0.85"/>
<vertex x="-1.25" y="-1.4"/>
<vertex x="-1.25" y="1.4"/>
</polygon>
</package>
<package name="CPOL_D">
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="0.95" x2="-3.25" y2="3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="0.95" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-0.95" x2="3.25" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-2.4" y="0" dx="3" dy="1.4" layer="1"/>
<smd name="+" x="2.4" y="0" dx="3" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL-G">
<wire x1="-5" y1="5" x2="3.23" y2="5" width="0.1016" layer="51"/>
<wire x1="3.23" y1="5" x2="4.79" y2="1.55" width="0.1016" layer="51"/>
<wire x1="4.79" y1="1.55" x2="4.79" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="4.79" y1="-1.55" x2="3.23" y2="-5" width="0.1016" layer="51"/>
<wire x1="3.23" y1="-5" x2="-5" y2="-5" width="0.1016" layer="51"/>
<wire x1="-5" y1="-5" x2="-5" y2="5" width="0.1016" layer="51"/>
<wire x1="-5" y1="0.95" x2="-5" y2="5" width="0.1016" layer="21"/>
<wire x1="-5" y1="5" x2="3.23" y2="5" width="0.1016" layer="21"/>
<wire x1="3.23" y1="5" x2="4.79" y2="1.55" width="0.1016" layer="21"/>
<wire x1="4.79" y1="1.55" x2="4.79" y2="0.95" width="0.1016" layer="21"/>
<wire x1="4.79" y1="1.59" x2="4.79" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="4.79" y1="-1.55" x2="3.23" y2="-5" width="0.1016" layer="21"/>
<wire x1="3.23" y1="-5" x2="-5" y2="-5" width="0.1016" layer="21"/>
<wire x1="-5" y1="-5" x2="-5" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-3.65" y="0" dx="3.2" dy="1.4" layer="1"/>
<smd name="+" x="3.65" y="0" dx="3.2" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL_F_8X10">
<wire x1="-4.1" y1="5.1" x2="1.5" y2="5.1" width="0.1016" layer="51"/>
<wire x1="1.5" y1="5.1" x2="4.1" y2="2.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="2.8" x2="4.1" y2="-2.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-2.8" x2="1.3" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="1.3" y1="-5.1" x2="-4.1" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-5.1" x2="-4.1" y2="5.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="1" x2="-4.1" y2="5.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="5.1" x2="1.5" y2="5.1" width="0.1016" layer="21"/>
<wire x1="1.5" y1="5.1" x2="4.1" y2="2.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="2.8" x2="4.1" y2="1" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-1" x2="4.1" y2="-2.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-2.8" x2="1.3" y2="-5.1" width="0.1016" layer="21"/>
<wire x1="1.3" y1="-5.1" x2="-4.1" y2="-5.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="-5.1" x2="-4.1" y2="-1" width="0.1016" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.1016" layer="21" curve="-153.684915"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.1016" layer="21" curve="153.684915"/>
<circle x="0" y="0" radius="4" width="0.001" layer="51"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3.55" y="0" dx="4" dy="1.6" layer="1"/>
<smd name="+" x="3.55" y="0" dx="4" dy="1.6" layer="1"/>
<text x="-1.75" y="1.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-2.375" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-4.85" y1="-0.45" x2="-3.9" y2="0.45" layer="51"/>
<rectangle x1="3.9" y1="-0.45" x2="4.85" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="CPOL_E">
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="0.9" x2="-4.1" y2="4.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="21"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="0.9" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-0.9" x2="4.1" y2="-1.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="21"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.1016" layer="21" curve="-153.684915"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.1016" layer="21" curve="153.684915"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3" y="0" dx="3.8" dy="1.4" layer="1"/>
<smd name="+" x="3" y="0" dx="3.8" dy="1.4" layer="1"/>
<text x="-1.8" y="1.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.8" y="-2.225" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5" y1="-0.35" x2="-3.8" y2="0.35" layer="51"/>
<rectangle x1="3.8" y1="-0.35" x2="4.5" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="CPOL-16X16">
<wire x1="-7.54" y1="8.5" x2="5.77" y2="8.5" width="0.1016" layer="51"/>
<wire x1="5.77" y1="8.5" x2="7.33" y2="6" width="0.1016" layer="51"/>
<wire x1="7.33" y1="6" x2="7.33" y2="-6" width="0.1016" layer="51"/>
<wire x1="7.33" y1="-6" x2="5.77" y2="-8.5" width="0.1016" layer="51"/>
<wire x1="5.77" y1="-8.5" x2="-7.54" y2="-8.5" width="0.1016" layer="51"/>
<wire x1="-7.54" y1="-8.5" x2="-7.54" y2="8.5" width="0.1016" layer="51"/>
<wire x1="-7.54" y1="0.95" x2="-7.54" y2="8.5" width="0.1016" layer="21"/>
<wire x1="-7.54" y1="8.5" x2="5.77" y2="8.5" width="0.1016" layer="21"/>
<wire x1="5.77" y1="8.5" x2="7.33" y2="6" width="0.1016" layer="21"/>
<wire x1="7.33" y1="6" x2="7.33" y2="-6" width="0.1016" layer="21"/>
<wire x1="7.33" y1="-6" x2="5.77" y2="-8.5" width="0.1016" layer="21"/>
<wire x1="5.77" y1="-8.5" x2="-7.54" y2="-8.5" width="0.1016" layer="21"/>
<wire x1="-7.54" y1="-8.5" x2="-7.54" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-5.4" y="0" dx="5.4" dy="1.4" layer="1"/>
<smd name="+" x="5.4" y="0" dx="5.4" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="EIA3528-KIT">
<wire x1="-0.9" y1="-1.6" x2="-3.1" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.6" x2="-3.1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.7" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.55" x2="3.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-1.2" x2="3.1" y2="1.25" width="0.2032" layer="21"/>
<wire x1="3.1" y1="1.25" x2="2.7" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.4" layer="21" style="longdash"/>
<smd name="C" x="-1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<smd name="A" x="1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="E5-10.5">
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="5.08" width="0.1524" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="4.699" y="2.7432" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.1242" y="-3.2258" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
</package>
<package name="E5-12.5">
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="6.5" width="0.1524" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="4.699" y="2.7432" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.1242" y="-3.2258" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
</package>
<package name="EIA7343">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.2032" layer="21"/>
</package>
<package name="C2917">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.2032" layer="21"/>
</package>
<package name="EIA3216">
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="E2-5">
<wire x1="-1.524" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.778" x2="-0.762" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="1.524" x2="-1.016" y2="2.032" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="-" x="1.016" y="0" drill="0.8128" diameter="1.27" shape="octagon"/>
<pad name="+" x="-1.016" y="0" drill="0.8128" diameter="1.27"/>
<text x="2.54" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.54" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="51"/>
</package>
<package name="E3.5-8">
<wire x1="-3.429" y1="1.143" x2="-2.667" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0.762" x2="-3.048" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="4.064" width="0.1524" layer="21"/>
<pad name="-" x="1.778" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.778" y="0" drill="0.8128" diameter="1.6002"/>
<text x="3.302" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="E2.5-6">
<wire x1="-2.159" y1="0" x2="-2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.254" x2="-2.413" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1524" layer="21"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.5748" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.5748"/>
<text x="2.667" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.667" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="SOT223">
<wire x1="-3.124" y1="1.731" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="-3.124" y1="1.731" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<smd name="1" x="-2.2606" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="2" x="0.0254" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="3" x="2.3114" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="4" x="0" y="3.1496" dx="3.81" dy="2.0066" layer="1"/>
<text x="-2.54" y="4.318" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.794" y="-5.842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="1.778" x2="1.524" y2="3.302" layer="51"/>
<rectangle x1="-2.667" y1="-3.302" x2="-1.905" y2="-1.778" layer="51"/>
<rectangle x1="1.905" y1="-3.302" x2="2.667" y2="-1.778" layer="51"/>
<rectangle x1="-0.381" y1="-3.302" x2="0.381" y2="-1.778" layer="51"/>
</package>
<package name="C0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.508" x2="-1.27" y2="0.508" width="0.127" layer="21"/>
</package>
<package name="FILM-CAPACITOR-0.47U/400V">
<wire x1="-9.25" y1="3.9" x2="9.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="3.9" x2="9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="-3.9" x2="-9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-3.9" x2="-9.25" y2="3.9" width="0.127" layer="21"/>
<pad name="P$1" x="-7.5" y="0" drill="0.9"/>
<pad name="P$2" x="7.5" y="0" drill="0.9"/>
<text x="-2.54" y="4.17" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.17" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
</package>
<package name="R0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-2.032" y="0.635" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.159" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.63" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.63" width="0.127" layer="21"/>
</package>
<package name="R-0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.3" y1="0.473" x2="-1.3" y2="-0.473" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-0.473" x2="1.2" y2="-0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.473" x2="1.2" y2="0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.473" x2="-1.3" y2="0.473" width="0.127" layer="21"/>
</package>
<package name="10555-METRIC">
<smd name="P$1" x="-4.85" y="0" dx="3.3" dy="2" layer="1"/>
<smd name="P$2" x="4.35" y="0" dx="3.3" dy="2" layer="1"/>
<wire x1="-6.75" y1="3" x2="-6.75" y2="-3" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-3" x2="6.25" y2="-3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3" x2="6.25" y2="3" width="0.127" layer="21"/>
<wire x1="6.25" y1="3" x2="-6.75" y2="3" width="0.127" layer="21"/>
<text x="-2.75" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805-METRIC">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="-0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.127" layer="21"/>
</package>
<package name="PINHEAD-2X5">
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
</package>
<package name="CRYSTAL-2PIN">
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CRYSTAL-4PIN">
<wire x1="-0.6" y1="1.6" x2="0.6" y2="1.6" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.3" x2="2.5" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.6" x2="-0.6" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.3" x2="-2.5" y2="-0.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.85" y="-1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="3" x="1.85" y="1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="GND@2" x="-1.85" y="1.15" dx="1.9" dy="1.1" layer="1"/>
<smd name="GND@1" x="1.85" y="-1.15" dx="1.9" dy="1.1" layer="1"/>
<text x="-2.54" y="1.905" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<circle x="-3.302" y="-1.27" radius="0.359209375" width="0.127" layer="21"/>
<circle x="-3.302" y="-1.27" radius="0.283978125" width="0.127" layer="21"/>
<circle x="-3.302" y="-1.2446" radius="0.193440625" width="0.127" layer="21"/>
<circle x="-3.2512" y="-1.2192" radius="0.1016" width="0.127" layer="21"/>
<circle x="-3.3528" y="-1.2446" radius="0.08031875" width="0.127" layer="21"/>
</package>
<package name="NX5032">
<wire x1="2.5" y1="-1.2172" x2="2.2172" y2="-1.6" width="0.127" layer="21" curve="-90"/>
<wire x1="2.2172" y1="-1.6" x2="-2.2172" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2172" y1="-1.6" x2="-2.5" y2="-1.2172" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.5" y1="-1.2172" x2="-2.5" y2="1.3764" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.3764" x2="-2.2764" y2="1.6" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.2764" y1="1.6" x2="2.1838" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.1838" y1="1.6" x2="2.5" y2="1.2838" width="0.127" layer="21" curve="-90"/>
<wire x1="2.5" y1="1.2838" x2="2.5" y2="-1.2172" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.3" x2="1.4" y2="1.3" width="0.127" layer="21"/>
<wire x1="1.4" y1="1.3" x2="2" y2="0.6" width="0.127" layer="21" curve="-89.967269"/>
<wire x1="2" y1="0.6" x2="2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="2" y1="-0.8" x2="1.4" y2="-1.3" width="0.127" layer="21" curve="-90"/>
<wire x1="1.4" y1="-1.3" x2="-1.3" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-1.3" x2="-2" y2="-0.7" width="0.127" layer="21" curve="-90"/>
<wire x1="-2" y1="-0.7" x2="-2" y2="0.7" width="0.127" layer="21"/>
<wire x1="-2" y1="0.7" x2="-1.4" y2="1.3" width="0.127" layer="21" curve="-90"/>
<smd name="P$1" x="2" y="0" dx="2" dy="2.4" layer="1"/>
<smd name="P$2" x="-2" y="0" dx="2" dy="2.4" layer="1" rot="R180"/>
<text x="-2.6" y="1.9" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3" y="-3.1" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="SJ_2">
<wire x1="2.159" y1="-1.016" x2="-2.159" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="0.762" x2="-2.159" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="-0.762" x2="-2.159" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.016" x2="2.413" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.762" x2="-2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.016" x2="2.159" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0" x2="-2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="0.762" x2="0" y2="1.016" width="0.1524" layer="51"/>
<wire x1="0" y1="-1.016" x2="0" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.016" y1="0.127" x2="1.016" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="-1.016" y1="-0.127" x2="-1.016" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-1.524" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="3" x="1.524" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-2.413" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.1001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.762" x2="0.508" y2="0.762" layer="51"/>
</package>
<package name="L0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="-0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.127" layer="21"/>
</package>
<package name="L0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="1A/250V_FUSE">
<smd name="P$1" x="-4.6736" y="0" dx="3.429" dy="3.2512" layer="1" rot="R90"/>
<smd name="P$2" x="4.6736" y="0" dx="3.429" dy="3.2512" layer="1" rot="R90"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="2.54" x2="-7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="-7.62" y1="1.778" x2="-7.62" y2="2.54" width="0.127" layer="51"/>
<wire x1="-7.62" y1="2.54" x2="-6.604" y2="2.54" width="0.127" layer="51"/>
<wire x1="6.604" y1="2.54" x2="7.62" y2="2.54" width="0.127" layer="51"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="1.778" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-1.778" x2="-7.62" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-2.54" x2="-6.604" y2="-2.54" width="0.127" layer="51"/>
<wire x1="6.604" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="51"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="-1.524" width="0.127" layer="51"/>
<text x="-3.048" y="2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CU3225K">
<wire x1="-3.5" y1="3.05" x2="3.5" y2="3.05" width="0.2032" layer="21"/>
<wire x1="3.5" y1="3.05" x2="3.5" y2="-3.05" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-3.05" x2="-3.5" y2="-3.05" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-3.05" x2="-3.5" y2="3.05" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-3.05" x2="-3.5" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-1.95" x2="3.5" y2="-3.05" width="0.2032" layer="21"/>
<wire x1="3.5" y1="3.05" x2="3.5" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="1.95" x2="-3.5" y2="3.05" width="0.2032" layer="21"/>
<smd name="1" x="-3.65" y="0" dx="2.8" dy="3.5" layer="1"/>
<smd name="2" x="3.65" y="0" dx="2.8" dy="3.5" layer="1"/>
<text x="-3.5" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.5" y="-5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4" y1="-1.5" x2="-3.6" y2="1.5" layer="51"/>
<rectangle x1="3.6" y1="-1.5" x2="4" y2="1.5" layer="51" rot="R180"/>
</package>
<package name="SMADIODE">
<wire x1="-2.15" y1="1.3" x2="2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.3" x2="2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.3" x2="-2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.3" x2="-2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-3.789" y1="-1.394" x2="-3.789" y2="-1.146" width="0.127" layer="21"/>
<wire x1="-3.789" y1="-1.146" x2="-3.789" y2="1.6" width="0.127" layer="21"/>
<wire x1="-3.789" y1="1.6" x2="3.816" y2="1.6" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.6" x2="3.816" y2="1.394" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="1.3365" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3.816" y1="-1.6" x2="-3.789" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-3.789" y1="-1.6" x2="-3.789" y2="-1.146" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="-0.4445" x2="-0.3175" y2="0.4445" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="0.4445" x2="-0.6985" y2="0" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0" x2="-0.3175" y2="-0.4445" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="-0.4445" x2="-0.6985" y2="0.4445" width="0.127" layer="21"/>
<smd name="C" x="-2.3495" y="0" dx="2.54" dy="2.54" layer="1"/>
<smd name="A" x="2.3495" y="0" dx="2.54" dy="2.54" layer="1" rot="R180"/>
<text x="-2.54" y="1.905" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-2.825" y1="-1.1" x2="-2.175" y2="1.1" layer="51"/>
<rectangle x1="2.175" y1="-1.1" x2="2.825" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.75" y1="-1.225" x2="-1.075" y2="1.225" layer="51"/>
</package>
<package name="PINHEAD-1X3">
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="-2.804" y1="-0.254" x2="-2.296" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-2.73" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="2.296" y1="-0.254" x2="2.804" y2="0.254" layer="51"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
</package>
<package name="LS_A67K">
<smd name="C" x="-1.27" y="0" dx="3.5" dy="1.5" layer="1" rot="R90"/>
<smd name="A" x="1.27" y="0" dx="3.5" dy="1.5" layer="1" rot="R90"/>
<wire x1="-2.5" y1="2" x2="-2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2" x2="2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2" x2="-2.5" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<circle x="0" y="1.524" radius="0.254" width="0.127" layer="21"/>
</package>
<package name="LS_Y876">
<smd name="A" x="1.5" y="0" dx="1" dy="0.7" layer="1" rot="R270"/>
<smd name="C" x="-1.5" y="0" dx="1" dy="0.7" layer="1" rot="R270"/>
<wire x1="-1.016" y1="-0.762" x2="1.016" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.762" x2="1.016" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.762" x2="-1.016" y2="0.762" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.762" x2="-1.016" y2="-0.762" width="0.127" layer="21"/>
<circle x="0" y="-0.254" radius="0.254" width="0.127" layer="21"/>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="LED0603">
<wire x1="0.45" y1="0.4" x2="-0.45" y2="0.4" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.4" x2="-0.45" y2="-0.4" width="0.1016" layer="51"/>
<smd name="C" x="0.85" y="0" dx="1" dy="1" layer="1" rot="R270"/>
<smd name="A" x="-0.85" y="0" dx="1" dy="1" layer="1" rot="R270"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.2" y1="-0.2" x2="1.1" y2="0.2" layer="51" rot="R270"/>
<rectangle x1="-1.1" y1="-0.2" x2="-0.2" y2="0.2" layer="51" rot="R270"/>
<rectangle x1="0.075" y1="0.225" x2="0.225" y2="0.525" layer="21" rot="R270"/>
<rectangle x1="0.075" y1="-0.525" x2="0.225" y2="-0.225" layer="21" rot="R270"/>
<rectangle x1="0" y1="-0.15" x2="0.3" y2="0.15" layer="21" rot="R270"/>
<wire x1="1.5" y1="0.7" x2="-1.6" y2="0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="1.5" y2="0.7" width="0.127" layer="21"/>
</package>
<package name="LED-5MM">
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="AVR_ICSP_PTH">
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-3.81" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.02" diameter="1.632" shape="square"/>
<pad name="2" x="-2.54" y="1.27" drill="1.02" diameter="1.632"/>
<pad name="3" x="0" y="-1.27" drill="1.02" diameter="1.632"/>
<pad name="4" x="0" y="1.27" drill="1.02" diameter="1.632"/>
<pad name="5" x="2.54" y="-1.27" drill="1.02" diameter="1.632"/>
<text x="-3.81" y="2.8575" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="21" font="vector">1</text>
<text x="-5.08" y="0.635" size="1.27" layer="21" font="vector">2</text>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="48"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="48"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="48"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="48"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="48"/>
<polygon width="0.0634" layer="21">
<vertex x="-2.54" y="-3.175"/>
<vertex x="-3.175" y="-3.81"/>
<vertex x="-1.905" y="-3.81"/>
</polygon>
<pad name="6" x="2.54" y="1.27" drill="1.02" diameter="1.632"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="48"/>
</package>
<package name="SJW">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
<package name="R2512">
<smd name="P$1" x="0" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<smd name="P$2" x="6.5024" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<wire x1="-1.27" y1="1.9304" x2="-1.27" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.9304" x2="7.7724" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="-1.9304" x2="7.7724" y2="1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="1.9304" x2="-1.27" y2="1.9304" width="0.127" layer="21"/>
<text x="1.016" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0.508" y="-3.556" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="5.334" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="-1.524" x2="5.334" y2="-1.524" width="0.127" layer="51"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="1.524" width="0.127" layer="51"/>
</package>
<package name="MELF0204">
<smd name="P$1" x="-0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.8" x2="1.2" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.8" x2="1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.8" x2="-1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.8" x2="-1.2" y2="0.8" width="0.127" layer="21"/>
<text x="-0.8" y="1.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.8" y="-2.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BOX">
</package>
<package name="HB1329">
<hole x="-57.75" y="90.75" drill="4.5"/>
<hole x="-57.75" y="-90.75" drill="4.5"/>
<hole x="57.75" y="-90.75" drill="4.5"/>
<hole x="57.75" y="90.75" drill="4.5"/>
<wire x1="-46.5" y1="104" x2="46.5" y2="104" width="0.254" layer="20"/>
<wire x1="65" y1="94.5" x2="65" y2="9" width="0.254" layer="20"/>
<wire x1="65" y1="-9" x2="65" y2="-94.5" width="0.254" layer="20"/>
<wire x1="46.5" y1="-104" x2="-46.5" y2="-104" width="0.254" layer="20"/>
<wire x1="-65" y1="-94.5" x2="-65" y2="-9" width="0.254" layer="20"/>
<wire x1="-65" y1="9" x2="-65" y2="94.5" width="0.254" layer="20"/>
<wire x1="-46.5" y1="-104" x2="-55" y2="-94.5" width="0.254" layer="20" curve="90"/>
<wire x1="-55" y1="-94.5" x2="-65" y2="-94.5" width="0.254" layer="20"/>
<wire x1="46.5" y1="104" x2="55" y2="94.5" width="0.254" layer="20" curve="90"/>
<wire x1="55" y1="94.5" x2="65" y2="94.5" width="0.254" layer="20"/>
<wire x1="46.5" y1="-104" x2="55" y2="-94.5" width="0.254" layer="20" curve="-90"/>
<wire x1="55" y1="-94.5" x2="65" y2="-94.5" width="0.254" layer="20"/>
<wire x1="-46.5" y1="104" x2="-55" y2="94.5" width="0.254" layer="20" curve="-90"/>
<wire x1="-55" y1="94.5" x2="-65" y2="94.5" width="0.254" layer="20"/>
<wire x1="-65" y1="-9" x2="-65" y2="9" width="0.254" layer="20" curve="180"/>
<wire x1="65" y1="9" x2="65" y2="-9" width="0.254" layer="20" curve="180"/>
<text x="-8" y="49" size="1.27" layer="25">&gt;NAME</text>
<text x="-6" y="-45" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="HB1340">
<hole x="-68.199" y="100.7491" drill="5.9944"/>
<hole x="-68.199" y="-100.7491" drill="5.9944"/>
<hole x="68.199" y="-100.7491" drill="5.9944"/>
<hole x="68.199" y="100.7491" drill="5.9944"/>
<wire x1="-57.785" y1="111.7092" x2="57.785" y2="111.7092" width="0.254" layer="20"/>
<wire x1="74.93" y1="104.49998125" x2="74.93" y2="10.16" width="0.254" layer="20"/>
<wire x1="74.93" y1="-10.16" x2="74.93" y2="-104.49998125" width="0.254" layer="20"/>
<wire x1="57.785" y1="-111.7092" x2="-57.785" y2="-111.7092" width="0.254" layer="20"/>
<wire x1="-74.93" y1="-104.49998125" x2="-74.93" y2="-10.16" width="0.254" layer="20"/>
<wire x1="-74.93" y1="10.16" x2="-74.93" y2="104.499978125" width="0.254" layer="20"/>
<wire x1="-57.785" y1="-111.7092" x2="-67.50188125" y2="-104.49998125" width="0.254" layer="20" curve="90"/>
<wire x1="-67.50188125" y1="-104.49998125" x2="-74.93" y2="-104.49998125" width="0.254" layer="20"/>
<wire x1="57.785" y1="111.7092" x2="67.50188125" y2="104.49998125" width="0.254" layer="20" curve="90"/>
<wire x1="67.50188125" y1="104.49998125" x2="74.93" y2="104.49998125" width="0.254" layer="20"/>
<wire x1="57.785" y1="-111.7092" x2="67.50188125" y2="-104.49998125" width="0.254" layer="20" curve="-90"/>
<wire x1="67.50188125" y1="-104.49998125" x2="74.93" y2="-104.49998125" width="0.254" layer="20"/>
<wire x1="-57.785" y1="111.7092" x2="-67.50188125" y2="104.49998125" width="0.254" layer="20" curve="-90"/>
<wire x1="-67.50188125" y1="104.49998125" x2="-74.93" y2="104.499978125" width="0.254" layer="20"/>
<wire x1="-74.93" y1="-10.16" x2="-74.93" y2="10.16" width="0.254" layer="20" curve="180"/>
<wire x1="74.93" y1="10.16" x2="74.93" y2="-10.16" width="0.254" layer="20" curve="180"/>
<text x="-8" y="49" size="1.27" layer="25">&gt;NAME</text>
<text x="-6" y="-45" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="2.4638"/>
<hole x="-68.199" y="-57.0103" drill="2.4638"/>
<hole x="68.199" y="-57.0103" drill="2.4638"/>
<hole x="-68.199" y="57.0103" drill="2.4638"/>
<hole x="68.199" y="57.0103" drill="2.4638"/>
</package>
<package name="1593BBGY">
<wire x1="-40.33" y1="27.305" x2="-24.75" y2="27.305" width="0.3048" layer="20"/>
<wire x1="-24.75" y1="27.305" x2="-24.75" y2="23.305" width="0.3048" layer="20"/>
<wire x1="-24.75" y1="23.305" x2="-20.435" y2="19.305" width="0.3048" layer="20" curve="90"/>
<wire x1="-20.435" y1="19.305" x2="-16.12" y2="23.305" width="0.3048" layer="20" curve="90"/>
<wire x1="-16.12" y1="23.305" x2="-16.12" y2="27.305" width="0.3048" layer="20"/>
<wire x1="31.623" y1="27.305" x2="24.75" y2="27.305" width="0.3048" layer="20"/>
<wire x1="24.75" y1="27.305" x2="24.75" y2="23.305" width="0.3048" layer="20"/>
<wire x1="24.75" y1="23.305" x2="20.435" y2="19.305" width="0.3048" layer="20" curve="-90"/>
<wire x1="20.435" y1="19.305" x2="16.12" y2="23.305" width="0.3048" layer="20" curve="-90"/>
<wire x1="16.12" y1="23.305" x2="16.12" y2="27.305" width="0.3048" layer="20"/>
<wire x1="-16.12" y1="27.305" x2="16.12" y2="27.305" width="0.3048" layer="20"/>
<wire x1="31.623" y1="-27.305" x2="24.75" y2="-27.305" width="0.3048" layer="20"/>
<wire x1="24.75" y1="-27.305" x2="24.75" y2="-23.305" width="0.3048" layer="20"/>
<wire x1="24.75" y1="-23.305" x2="20.435" y2="-19.305" width="0.3048" layer="20" curve="90"/>
<wire x1="20.435" y1="-19.305" x2="16.12" y2="-23.305" width="0.3048" layer="20" curve="90"/>
<wire x1="16.12" y1="-23.305" x2="16.12" y2="-27.305" width="0.3048" layer="20"/>
<wire x1="-40.33" y1="-27.305" x2="-24.75" y2="-27.305" width="0.3048" layer="20"/>
<wire x1="-24.75" y1="-27.305" x2="-24.75" y2="-23.305" width="0.3048" layer="20"/>
<wire x1="-24.75" y1="-23.305" x2="-20.435" y2="-19.305" width="0.3048" layer="20" curve="-90"/>
<wire x1="-20.435" y1="-19.305" x2="-16.12" y2="-23.305" width="0.3048" layer="20" curve="-90"/>
<wire x1="-16.12" y1="-23.305" x2="-16.12" y2="-27.305" width="0.3048" layer="20"/>
<wire x1="16.12" y1="-27.305" x2="-16.12" y2="-27.305" width="0.3048" layer="20"/>
<wire x1="-40.33" y1="27.305" x2="-40.33" y2="-27.305" width="0.3048" layer="20"/>
<wire x1="31.623" y1="27.305" x2="31.623" y2="-27.305" width="0.3048" layer="20"/>
<hole x="27" y="-14" drill="3.3"/>
<hole x="27" y="14" drill="3.3"/>
<text x="-3" y="12" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-20" size="1.27" layer="27">&gt;VALUE</text>
<circle x="27" y="14" radius="4" width="0.127" layer="21"/>
<circle x="27" y="-14" radius="4" width="0.127" layer="21"/>
</package>
<package name="SCREW_CONNECTOR-2PIN-2.54MM">
<pad name="P$1" x="0" y="0" drill="1.016" diameter="2.032"/>
<pad name="P$2" x="2.54" y="0" drill="1.016" diameter="2.032"/>
<wire x1="-1.47" y1="3.25" x2="-1.47" y2="-3.25" width="0.127" layer="21"/>
<wire x1="4.11" y1="3.25" x2="4.11" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-1.47" y1="3.25" x2="4.11" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.47" y1="-3.25" x2="4.11" y2="-3.25" width="0.127" layer="21"/>
<text x="-1.27" y="-5.28" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SCREW_CONNECTOR-2PIN-5.08MM">
<pad name="P$1" x="0" y="0" drill="1.3" diameter="2.54"/>
<pad name="P$2" x="5.08" y="0" drill="1.3" diameter="2.54"/>
<wire x1="-3.1" y1="4.2" x2="-3.1" y2="-4.2" width="0.127" layer="21"/>
<wire x1="8.18" y1="4.2" x2="8.18" y2="-4.2" width="0.127" layer="21"/>
<wire x1="-3.1" y1="4.2" x2="8.18" y2="4.2" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-4.2" x2="8.18" y2="-4.2" width="0.127" layer="21"/>
<text x="-1.27" y="-5.788" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-3.1" y1="3.4" x2="8.18" y2="3.4" width="0.127" layer="51"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.905" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="3.175" y2="1.905" width="0.127" layer="21"/>
</package>
<package name="SCREW_CONNECTOR-2PIN-3.5MM">
<pad name="P$1" x="-1.75" y="0" drill="1.016" diameter="2.032"/>
<pad name="P$2" x="1.75" y="0" drill="1.016" diameter="2.032"/>
<wire x1="-3.5" y1="3.1" x2="-3.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.1" x2="3.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.1" x2="3.5" y2="3.1" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-4.5" x2="3.5" y2="-4.5" width="0.127" layer="21"/>
<text x="-2.54" y="-5.28" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SHROUDED-TERMINAL-BLOCK-3.5MM">
<pad name="P$1" x="-1.75" y="0" drill="1.016" diameter="2.032"/>
<pad name="P$2" x="1.75" y="0" drill="1.016" diameter="2.032"/>
<wire x1="-4.25" y1="1.5" x2="-4.25" y2="-8" width="0.127" layer="21"/>
<wire x1="4.25" y1="1.5" x2="4.25" y2="-8" width="0.127" layer="21"/>
<wire x1="-4.25" y1="1.5" x2="4.25" y2="1.5" width="0.127" layer="21"/>
<wire x1="-4.25" y1="-8" x2="4.25" y2="-8" width="0.127" layer="21"/>
<text x="-2.54" y="-7.82" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="1.27MM_HEADER-SMT">
<smd name="9" x="0" y="-2.54" dx="2.115" dy="0.65" layer="1"/>
<smd name="10" x="3.385" y="-2.54" dx="2.115" dy="0.65" layer="1"/>
<smd name="7" x="0" y="-1.27" dx="2.115" dy="0.65" layer="1"/>
<smd name="8" x="3.385" y="-1.27" dx="2.115" dy="0.65" layer="1"/>
<smd name="5" x="0" y="0" dx="2.115" dy="0.65" layer="1"/>
<smd name="6" x="3.385" y="0" dx="2.115" dy="0.65" layer="1"/>
<smd name="3" x="0" y="1.27" dx="2.115" dy="0.65" layer="1"/>
<smd name="4" x="3.385" y="1.27" dx="2.115" dy="0.65" layer="1"/>
<smd name="1" x="0" y="2.54" dx="2.115" dy="0.65" layer="1"/>
<smd name="2" x="3.385" y="2.54" dx="2.115" dy="0.65" layer="1"/>
<wire x1="-1.27" y1="3.302" x2="-1.27" y2="-3.302" width="0.05" layer="21"/>
<wire x1="-1.27" y1="-3.302" x2="4.572" y2="-3.302" width="0.05" layer="21"/>
<wire x1="4.572" y1="-3.302" x2="4.572" y2="3.302" width="0.05" layer="21"/>
<wire x1="4.572" y1="3.302" x2="-1.27" y2="3.302" width="0.05" layer="21"/>
<circle x="0" y="3.1" radius="0.1" width="0.127" layer="21"/>
<circle x="0" y="3.1" radius="0.1" width="0.127" layer="21"/>
<circle x="0" y="3.1" radius="0.1" width="0" layer="21"/>
<circle x="0" y="3.1" radius="0.22360625" width="0" layer="21"/>
</package>
<package name="RESISTOR-ARRAY-4-1206">
<wire x1="-1.55" y1="0.75" x2="-1.35" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.075" y1="0.75" x2="-0.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-0.275" y1="0.75" x2="0.25" y2="0.75" width="0.1016" layer="51"/>
<wire x1="0.525" y1="0.75" x2="1.05" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.325" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="1.35" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="1.075" y1="-0.75" x2="0.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="0.275" y1="-0.75" x2="-0.25" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.525" y1="-0.75" x2="-1.05" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.325" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.35" y1="0.75" x2="-1.075" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-0.55" y1="0.75" x2="-0.275" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="0.25" y1="0.75" x2="0.525" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="1.05" y1="0.75" x2="1.325" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="1.35" y1="-0.75" x2="1.075" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="0.55" y1="-0.75" x2="0.275" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-0.25" y1="-0.75" x2="-0.525" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-1.05" y1="-0.75" x2="-1.325" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<smd name="1" x="-1.2" y="-0.725" dx="0.5" dy="0.65" layer="1"/>
<smd name="2" x="-0.4" y="-0.725" dx="0.5" dy="0.65" layer="1"/>
<smd name="3" x="0.4" y="-0.725" dx="0.5" dy="0.65" layer="1"/>
<smd name="4" x="1.2" y="-0.725" dx="0.5" dy="0.65" layer="1"/>
<smd name="5" x="1.2" y="0.725" dx="0.5" dy="0.65" layer="1"/>
<smd name="6" x="0.4" y="0.725" dx="0.5" dy="0.65" layer="1"/>
<smd name="7" x="-0.4" y="0.725" dx="0.5" dy="0.65" layer="1"/>
<smd name="8" x="-1.2" y="0.725" dx="0.5" dy="0.65" layer="1"/>
<text x="-1.905" y="-2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ESP8266-07">
<smd name="TXD" x="-6" y="8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="RST" x="-6" y="-8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="RXD" x="-4" y="8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO4" x="-2" y="8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO5" x="0" y="8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO0" x="2" y="8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO2" x="4" y="8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO15" x="6" y="8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="ADC" x="-4" y="-8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="CH_PD" x="-2" y="-8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO16" x="0" y="-8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO14" x="2" y="-8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO12" x="4" y="-8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="GPIO13" x="6" y="-8" dx="2" dy="0.635" layer="1" rot="R90"/>
<wire x1="-12" y1="8" x2="-11" y2="8" width="0.127" layer="21"/>
<wire x1="-11" y1="8" x2="-10.25" y2="8" width="0.127" layer="21"/>
<wire x1="-10.25" y1="8" x2="-9.5" y2="8" width="0.127" layer="21"/>
<wire x1="-9.5" y1="8" x2="-8.5" y2="8" width="0.127" layer="21"/>
<wire x1="-8.5" y1="8" x2="-7.5" y2="8" width="0.127" layer="21"/>
<wire x1="-7.5" y1="8" x2="10" y2="8" width="0.127" layer="21"/>
<wire x1="10" y1="8" x2="10" y2="-8" width="0.127" layer="21"/>
<wire x1="10" y1="-8" x2="-9" y2="-8" width="0.127" layer="21"/>
<wire x1="-9" y1="-8" x2="-12" y2="-8" width="0.127" layer="21"/>
<wire x1="-12" y1="-8" x2="-12" y2="-5" width="0.127" layer="21"/>
<text x="-4" y="4" size="1.27" layer="25">&gt;NAME</text>
<text x="-4" y="-4" size="1.27" layer="27">&gt;VALUE</text>
<smd name="GND" x="8" y="8" dx="2" dy="0.635" layer="1" rot="R90"/>
<smd name="VCC" x="8" y="-8" dx="2" dy="0.635" layer="1" rot="R90"/>
<circle x="-10.16" y="-6.096" radius="1.481059375" width="0.127" layer="21"/>
<circle x="-10.16" y="-6.096" radius="0.567959375" width="0.127" layer="21"/>
<circle x="-10.16" y="-6.096" radius="0.359209375" width="0.127" layer="21"/>
<circle x="-10.16" y="-6.096" radius="0.254" width="0.127" layer="21"/>
<wire x1="-12" y1="-5" x2="-12" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-12" y1="-3.5" x2="-12" y2="-2" width="0.127" layer="21"/>
<wire x1="-12" y1="-2" x2="-12" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-12" y1="-0.5" x2="-12" y2="1" width="0.127" layer="21"/>
<wire x1="-12" y1="1" x2="-12" y2="2.5" width="0.127" layer="21"/>
<wire x1="-12" y1="2.5" x2="-12" y2="4" width="0.127" layer="21"/>
<wire x1="-12" y1="4" x2="-12" y2="5.25" width="0.127" layer="21"/>
<wire x1="-12" y1="5.25" x2="-12" y2="6.5" width="0.127" layer="21"/>
<wire x1="-12" y1="6.5" x2="-12" y2="8" width="0.127" layer="21"/>
<wire x1="-18" y1="-8" x2="-18" y2="8" width="0.127" layer="51"/>
<wire x1="-18" y1="8" x2="-12" y2="8" width="0.127" layer="51"/>
<wire x1="-18" y1="-8" x2="-12" y2="-8" width="0.127" layer="51"/>
<wire x1="-7" y1="8" x2="-7" y2="-8" width="0.127" layer="39"/>
<wire x1="-7" y1="-8" x2="-18" y2="-8" width="0.127" layer="39"/>
<wire x1="-18" y1="-8" x2="-18" y2="8" width="0.127" layer="39"/>
<wire x1="-18" y1="8" x2="-7" y2="8" width="0.127" layer="39"/>
<wire x1="-11" y1="8" x2="-12" y2="6.5" width="0.127" layer="21"/>
<wire x1="-10.25" y1="8" x2="-12" y2="5.25" width="0.127" layer="21"/>
<wire x1="-9.5" y1="8" x2="-12" y2="4" width="0.127" layer="21"/>
<wire x1="-8.5" y1="8" x2="-12" y2="2.5" width="0.127" layer="21"/>
<wire x1="-7.5" y1="8" x2="-12" y2="1" width="0.127" layer="21"/>
<wire x1="-12" y1="-0.5" x2="-7" y2="7" width="0.127" layer="21"/>
<wire x1="-12" y1="-2" x2="-7" y2="5" width="0.127" layer="21"/>
<wire x1="-12" y1="-3.5" x2="-7" y2="3" width="0.127" layer="21"/>
<wire x1="-12" y1="-5" x2="-7" y2="1" width="0.127" layer="21"/>
<wire x1="-9.5" y1="-4" x2="-7" y2="-1" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-5" x2="-7" y2="-3" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-6.5" x2="-7" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-9" y1="-8" x2="-7" y2="-6" width="0.127" layer="21"/>
</package>
<package name="SHROUDED-TERMINAL-BLOCK-5MM">
<pad name="P$1" x="-2.5" y="0" drill="1.016" diameter="2.032"/>
<pad name="P$2" x="2.5" y="0" drill="1.016" diameter="2.032"/>
<wire x1="-6" y1="2" x2="-6" y2="-10" width="0.127" layer="21"/>
<wire x1="6" y1="2" x2="6" y2="-10" width="0.127" layer="21"/>
<wire x1="-6" y1="2" x2="6" y2="2" width="0.127" layer="21"/>
<wire x1="-6" y1="-10" x2="6" y2="-10" width="0.127" layer="21"/>
<text x="-2.54" y="-7.82" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SML-LX0404SIUPGUSB">
<smd name="4" x="-0.45" y="0.45" dx="0.5" dy="0.5" layer="1" rot="R90"/>
<smd name="1" x="0.45" y="0.45" dx="0.5" dy="0.5" layer="1" rot="R90"/>
<smd name="3" x="-0.45" y="-0.45" dx="0.5" dy="0.5" layer="1" rot="R90"/>
<smd name="2" x="0.45" y="-0.45" dx="0.5" dy="0.5" layer="1" rot="R90"/>
<wire x1="-0.8" y1="0.8" x2="-0.8" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-0.8" x2="0.8" y2="-0.8" width="0.127" layer="21"/>
<wire x1="0.8" y1="-0.8" x2="0.8" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.8" y1="0.8" x2="-0.8" y2="0.8" width="0.127" layer="21"/>
<wire x1="-0.05" y1="0.1" x2="0.1" y2="-0.05" width="0.06" layer="21"/>
<wire x1="0.1" y1="-0.05" x2="0.1" y2="0.1" width="0.06" layer="21"/>
<wire x1="0.1" y1="0.1" x2="-0.05" y2="0.1" width="0.06" layer="21"/>
<wire x1="-0.05" y1="0.1" x2="0.05" y2="0.05" width="0.06" layer="21"/>
<wire x1="-0.05" y1="0.1" x2="0.1" y2="-0.05" width="0.06" layer="51"/>
<wire x1="0.1" y1="-0.05" x2="0.1" y2="0.1" width="0.06" layer="51"/>
<wire x1="0.1" y1="0.1" x2="-0.05" y2="0.1" width="0.06" layer="51"/>
<wire x1="-0.05" y1="0.1" x2="0.05" y2="0.05" width="0.06" layer="51"/>
<wire x1="-0.2" y1="0.8" x2="-0.8" y2="0.8" width="0.06" layer="51"/>
<wire x1="-0.8" y1="0.8" x2="-0.8" y2="0.2" width="0.06" layer="51"/>
<wire x1="0.2" y1="0.8" x2="0.8" y2="0.8" width="0.06" layer="51"/>
<wire x1="0.8" y1="0.8" x2="0.8" y2="0.2" width="0.06" layer="51"/>
<wire x1="-0.8" y1="-0.2" x2="-0.8" y2="-0.8" width="0.06" layer="51"/>
<wire x1="-0.8" y1="-0.8" x2="-0.2" y2="-0.8" width="0.06" layer="51"/>
<wire x1="0.8" y1="-0.2" x2="0.8" y2="-0.8" width="0.06" layer="51"/>
<wire x1="0.8" y1="-0.8" x2="0.2" y2="-0.8" width="0.06" layer="51"/>
<text x="-0.6" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.8" y="-2.2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RGB-LED_5MM">
<pad name="P$1" x="-3.81" y="0" drill="0.8"/>
<pad name="P$2" x="-1.27" y="0" drill="0.8"/>
<pad name="P$3" x="1.27" y="0" drill="0.8"/>
<pad name="P$4" x="3.81" y="0" drill="0.8"/>
<circle x="0" y="0" radius="4.57905" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TACTILE">
<smd name="1" x="2.24" y="4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="2.24" y="-4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-2.24" y="-4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="-2.24" y="4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-3.175" y1="5.715" x2="3.175" y2="5.715" width="0.127" layer="21"/>
<wire x1="3.175" y1="5.715" x2="3.175" y2="-5.715" width="0.127" layer="21"/>
<wire x1="3.175" y1="-5.715" x2="-3.175" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-5.715" x2="-3.175" y2="5.715" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="TACTILE-PTH">
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="STPM34">
<pin name="CLKOUT/ZCR" x="-30.48" y="10.16" length="middle"/>
<pin name="CLKIN/XTAL2" x="-30.48" y="7.62" length="middle"/>
<pin name="XTAL1" x="-30.48" y="5.08" length="middle"/>
<pin name="LED1" x="-30.48" y="2.54" length="middle"/>
<pin name="LED2" x="-30.48" y="0" length="middle"/>
<pin name="INT1" x="-30.48" y="-2.54" length="middle"/>
<pin name="INT2" x="-30.48" y="-5.08" length="middle"/>
<pin name="EN" x="-30.48" y="-7.62" length="middle"/>
<pin name="VIP1" x="-7.62" y="-17.78" length="middle" rot="R90"/>
<pin name="VIN1" x="-5.08" y="-17.78" length="middle" rot="R90"/>
<pin name="IIP1" x="-2.54" y="-17.78" length="middle" rot="R90"/>
<pin name="IIN1" x="0" y="-17.78" length="middle" rot="R90"/>
<pin name="IIN2" x="2.54" y="-17.78" length="middle" rot="R90"/>
<pin name="IIP2" x="5.08" y="-17.78" length="middle" rot="R90"/>
<pin name="VIN2" x="7.62" y="-17.78" length="middle" rot="R90"/>
<pin name="VIP2" x="10.16" y="-17.78" length="middle" rot="R90"/>
<pin name="VREF1" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="GND_REF" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="VREF2" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="GNDA" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="VDDA" x="30.48" y="2.54" length="middle" rot="R180"/>
<pin name="GND_REG" x="30.48" y="5.08" length="middle" rot="R180"/>
<pin name="VCC" x="30.48" y="7.62" length="middle" rot="R180"/>
<pin name="NC" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="GNDD@0" x="10.16" y="22.86" length="middle" rot="R270"/>
<pin name="GNDD@1" x="7.62" y="22.86" length="middle" rot="R270"/>
<pin name="VDDD" x="5.08" y="22.86" length="middle" rot="R270"/>
<pin name="SYN" x="2.54" y="22.86" length="middle" rot="R270"/>
<pin name="SCS" x="0" y="22.86" length="middle" rot="R270"/>
<pin name="SCL" x="-2.54" y="22.86" length="middle" rot="R270"/>
<pin name="MOSI/RXD" x="-5.08" y="22.86" length="middle" rot="R270"/>
<pin name="MISO/TXD" x="-7.62" y="22.86" length="middle" rot="R270"/>
<wire x1="-25.4" y1="17.78" x2="-25.4" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-12.7" x2="25.4" y2="-12.7" width="0.254" layer="94"/>
<wire x1="25.4" y1="-12.7" x2="25.4" y2="17.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="17.78" x2="-25.4" y2="17.78" width="0.254" layer="94"/>
<text x="-5.08" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="HLK-PM01">
<pin name="L" x="-15.24" y="5.08" length="middle"/>
<pin name="N" x="-15.24" y="-5.08" length="middle"/>
<pin name="-VO" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="+VO" x="15.24" y="5.08" length="middle" rot="R180"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ATMEGA328">
<wire x1="-20.32" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="-30.48" x2="-20.32" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-30.48" x2="-20.32" y2="33.02" width="0.254" layer="94"/>
<text x="-20.32" y="33.782" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PB5(SCK)" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2)" x="-25.4" y="0" length="middle"/>
<pin name="PB6(XTAL1/TOSC1)" x="-25.4" y="5.08" length="middle"/>
<pin name="GND@3" x="-25.4" y="-22.86" length="middle"/>
<pin name="GND@5" x="-25.4" y="-25.4" length="middle"/>
<pin name="VCC@4" x="-25.4" y="22.86" length="middle"/>
<pin name="VCC@6" x="-25.4" y="20.32" length="middle"/>
<pin name="AGND" x="-25.4" y="-20.32" length="middle"/>
<pin name="AREF" x="-25.4" y="15.24" length="middle"/>
<pin name="AVCC" x="-25.4" y="25.4" length="middle"/>
<pin name="PB4(MISO)" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2)" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B)" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PB1(OC1A)" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="PB0(ICP)" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PD6(AIN0)" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PD5(T1)" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PD4(XCK/T0)" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PD3(INT1)" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="PD2(INT0)" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="PD1(TXD)" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PD0(RXD)" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="ADC7" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="ADC6" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL)" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA)" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="PC3(ADC3)" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PC2(ADC2)" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="PC1(ADC1)" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PC0(ADC0)" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="PC6(/RESET)" x="-25.4" y="30.48" length="middle" function="dot"/>
</symbol>
<symbol name="CPOL">
<wire x1="-1.524" y1="1.651" x2="1.524" y2="1.651" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.651" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.54" x2="-1.524" y2="1.651" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.54" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<text x="1.143" y="3.0226" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="2.9464" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-2.0574" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0" x2="1.651" y2="0.889" layer="94"/>
<pin name="-" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" visible="pad" length="short" direction="sup" rot="R270"/>
<text x="2.54" y="0" size="1.27" layer="95" font="vector" rot="R90">GND</text>
</symbol>
<symbol name="REG1117">
<wire x1="-10.16" y1="2.54" x2="10.16" y2="2.54" width="0.4064" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="10.16" width="0.4064" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="2.54" width="0.4064" layer="94"/>
<text x="-10.16" y="10.795" size="1.778" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" ratio="10">&gt;VALUE</text>
<pin name="VIN" x="-15.24" y="7.62" length="middle" direction="in"/>
<pin name="VOUT" x="15.24" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="GND" x="0" y="-2.54" length="middle" direction="pwr" rot="R90"/>
</symbol>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
<symbol name="3V3">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<text x="-2.54" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-2.54" y="-3.81" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="PINHEAD-2X5">
<wire x1="-6.35" y1="-7.62" x2="8.89" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="CRYSTAL">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.54" y="2.286" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="pad" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="pad" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="SJ_2">
<wire x1="-0.635" y1="-1.397" x2="0.635" y2="-1.397" width="1.27" layer="94" curve="180" cap="flat"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<text x="2.54" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.635" x2="1.27" y2="0.635" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="FUSE">
<rectangle x1="-5.08" y1="-2.54" x2="5.08" y2="2.54" layer="94"/>
<pin name="P$1" x="-10.16" y="0" visible="off" length="middle"/>
<pin name="P$2" x="10.16" y="0" visible="off" length="middle" rot="R180"/>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
</symbol>
<symbol name="VARISTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TVS-DIODE">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" rot="R180"/>
<pin name="A" x="-7.62" y="0" visible="off" length="short"/>
<text x="-5.08" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="5V">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="PINHEAD-1X3">
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<wire x1="1.27" y1="-5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
<symbol name="AVR_ICSP">
<wire x1="-8.89" y1="-5.08" x2="-8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-8.89" y1="5.08" x2="8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="-8.89" y2="-5.08" width="0.4064" layer="94"/>
<text x="-8.89" y="5.842" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-0.254" y="3.302" size="1.27" layer="95" font="vector" ratio="12" rot="R180">MISO</text>
<text x="-1.016" y="0.762" size="1.27" layer="95" font="vector" ratio="12" rot="R180">SCK</text>
<text x="-1.016" y="-1.778" size="1.27" layer="95" font="vector" ratio="12" rot="R180">RST</text>
<text x="4.826" y="3.302" size="1.27" layer="95" font="vector" ratio="12" rot="R180">+V</text>
<text x="4.826" y="0.762" size="1.27" layer="95" font="vector" ratio="12" rot="R180">MOSI</text>
<text x="4.826" y="-1.778" size="1.27" layer="95" font="vector" ratio="12" rot="R180">GND</text>
<pin name="+V" x="7.62" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="MISO" x="-7.62" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="SCK" x="-7.62" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="RST" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="MOSI" x="7.62" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GND" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="FERRITE-BEAD">
<wire x1="-2.54" y1="-0.635" x2="2.54" y2="-0.635" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.635" x2="2.54" y2="0.635" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.635" x2="-2.54" y2="0.635" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.254" layer="94"/>
<text x="-4.064" y="3.302" size="1.778" layer="95">&gt;Name</text>
<text x="-4.572" y="-5.08" size="1.778" layer="96">&gt;Value</text>
<pin name="P$1" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="P$2" x="-5.08" y="0" visible="off" length="short"/>
</symbol>
<symbol name="ISOLATED_GND">
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.667" y="-5.715" size="1.778" layer="96">&gt;VALUE</text>
<pin name="AGND" x="0" y="0" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="JUMPER">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="BOX">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="SCREW_CONNECTOR-2PIN">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="2.54" width="0.254" layer="94"/>
<pin name="1" x="0" y="7.62" length="middle" rot="R270"/>
<pin name="2" x="5.08" y="7.62" length="middle" rot="R270"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<text x="10.16" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="10.16" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="R-ARRAY-4">
<wire x1="-2.54" y1="-0.762" x2="2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.762" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.762" x2="-2.54" y2="-0.762" width="0.254" layer="94"/>
<text x="1.27" y="2.54" size="1.016" layer="96">&gt;VALUE</text>
<text x="-3.81" y="2.54" size="1.016" layer="95">&gt;NAME</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="ESP07">
<pin name="RESET" x="-17.78" y="15.24" length="middle"/>
<pin name="ADC" x="-17.78" y="10.16" length="middle"/>
<pin name="GPIO12" x="-17.78" y="-10.16" length="middle"/>
<pin name="GPIO13" x="-17.78" y="-15.24" length="middle"/>
<pin name="GPIO14" x="-17.78" y="-5.08" length="middle"/>
<pin name="GPIO5" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="GPIO4" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="GPIO2" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="CH_PD" x="-17.78" y="5.08" length="middle"/>
<pin name="GPIO16" x="-17.78" y="0" length="middle"/>
<pin name="URXD" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="UTXD" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="GPIO0" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="GND" x="17.78" y="-20.32" length="middle" rot="R180"/>
<wire x1="-12.7" y1="17.78" x2="-12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.27" layer="96">VALUE</text>
<pin name="VCC" x="-17.78" y="-20.32" length="middle"/>
<pin name="GPIO15" x="17.78" y="-15.24" length="middle" rot="R180"/>
</symbol>
<symbol name="RGBLED-CA">
<wire x1="1.27" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="1.27" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="1.27" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="-1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-6.35" y="1.27" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="2.54" y="-3.81" size="1.778" layer="94">Red</text>
<text x="2.54" y="1.27" size="1.778" layer="94">Grn</text>
<text x="2.54" y="6.35" size="1.778" layer="94">Blue</text>
<pin name="BLUE" x="7.62" y="5.08" visible="off" length="middle" rot="R180"/>
<pin name="GREEN" x="7.62" y="0" visible="off" length="middle" rot="R180"/>
<pin name="VCC" x="-10.16" y="0" visible="off" length="middle"/>
<pin name="RED" x="7.62" y="-5.08" visible="off" length="middle" rot="R180"/>
</symbol>
<symbol name="TACTILE">
<wire x1="-2.54" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="0.635" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="0" y2="0.635" width="0.254" layer="94"/>
<pin name="1" x="-7.62" y="0" visible="pin" length="short"/>
<pin name="2" x="-7.62" y="-2.54" visible="pin" length="short"/>
<pin name="3" x="7.62" y="0" visible="pin" length="short" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="0" y1="0.635" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<text x="7.62" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="7.62" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="STPM34" prefix="STPM34-">
<gates>
<gate name="G$1" symbol="STPM34" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN32">
<connects>
<connect gate="G$1" pin="CLKIN/XTAL2" pad="2"/>
<connect gate="G$1" pin="CLKOUT/ZCR" pad="1"/>
<connect gate="G$1" pin="EN" pad="8"/>
<connect gate="G$1" pin="GNDA" pad="20"/>
<connect gate="G$1" pin="GNDD@0" pad="25"/>
<connect gate="G$1" pin="GNDD@1" pad="26"/>
<connect gate="G$1" pin="GND_REF" pad="18"/>
<connect gate="G$1" pin="GND_REG" pad="22"/>
<connect gate="G$1" pin="IIN1" pad="12"/>
<connect gate="G$1" pin="IIN2" pad="13"/>
<connect gate="G$1" pin="IIP1" pad="11"/>
<connect gate="G$1" pin="IIP2" pad="14"/>
<connect gate="G$1" pin="INT1" pad="6"/>
<connect gate="G$1" pin="INT2" pad="7"/>
<connect gate="G$1" pin="LED1" pad="4"/>
<connect gate="G$1" pin="LED2" pad="5"/>
<connect gate="G$1" pin="MISO/TXD" pad="32"/>
<connect gate="G$1" pin="MOSI/RXD" pad="31"/>
<connect gate="G$1" pin="NC" pad="24"/>
<connect gate="G$1" pin="SCL" pad="30"/>
<connect gate="G$1" pin="SCS" pad="29"/>
<connect gate="G$1" pin="SYN" pad="28"/>
<connect gate="G$1" pin="VCC" pad="23"/>
<connect gate="G$1" pin="VDDA" pad="21"/>
<connect gate="G$1" pin="VDDD" pad="27"/>
<connect gate="G$1" pin="VIN1" pad="10"/>
<connect gate="G$1" pin="VIN2" pad="15"/>
<connect gate="G$1" pin="VIP1" pad="9"/>
<connect gate="G$1" pin="VIP2" pad="16"/>
<connect gate="G$1" pin="VREF1" pad="17"/>
<connect gate="G$1" pin="VREF2" pad="19"/>
<connect gate="G$1" pin="XTAL1" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="497-14670-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="2.41" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HLK-PM01">
<gates>
<gate name="G$1" symbol="HLK-PM01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HLK-PM01">
<connects>
<connect gate="G$1" pin="+VO" pad="+VO"/>
<connect gate="G$1" pin="-VO" pad="-VO"/>
<connect gate="G$1" pin="L" pad="L"/>
<connect gate="G$1" pin="N" pad="N"/>
</connects>
<technologies>
<technology name="">
<attribute name="LINK" value="http://www.hlktech.net/product_detail.php?ProId=54" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="2.5" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA328P" prefix="U">
<gates>
<gate name="G$1" symbol="ATMEGA328" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP-32">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AGND" pad="21"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="PB0(ICP)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1A)" pad="13"/>
<connect gate="G$1" pin="PB2(SS/OC1B)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI/OC2)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1/TOSC1)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2/TOSC2)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4/SDA)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5/SCL)" pad="28"/>
<connect gate="G$1" pin="PC6(/RESET)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD)" pad="31"/>
<connect gate="G$1" pin="PD2(INT0)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1)" pad="1"/>
<connect gate="G$1" pin="PD4(XCK/T0)" pad="2"/>
<connect gate="G$1" pin="PD5(T1)" pad="9"/>
<connect gate="G$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="G$1" pin="VCC@4" pad="4"/>
<connect gate="G$1" pin="VCC@6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="ATMEGA328P-AURCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="3.7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CPOL" prefix="CPOL" uservalue="yes">
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="-2.54"/>
</gates>
<devices>
<device name="_B" package="CPOL_B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_D" package="CPOL_D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="CPOL-G">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-F" package="CPOL_F_8X10">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-E" package="CPOL_E">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-16X16" package="CPOL-16X16">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TANTALUM-100UF/6.3V" package="EIA3528-KIT">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="478-8156-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.61"/>
</technology>
</technologies>
</device>
<device name="-470UF/25V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1189-2584-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.56"/>
</technology>
</technologies>
</device>
<device name="-680UF/35V-0.041OHM" package="E5-12.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="495-6062-ND"/>
<attribute name="PRICE_PER_UNIT" value="1.77"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_100UF/10V/0.08OHM" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-8497-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="1.49"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_330UF/6.3V/0.04OHM" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-10400-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.69"/>
</technology>
</technologies>
</device>
<device name="-1000UF/10V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="493-10963-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.4"/>
</technology>
</technologies>
</device>
<device name="-220UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6109-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.32" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6592-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.43" constant="no"/>
</technology>
</technologies>
</device>
<device name="-330UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="493-1083-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.37" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680UF/35V-19MM" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="565-1579-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.63" constant="no"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_220UF/16V" package="C2917">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-10429-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM-10UF/6.3V-1206" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="495-2181-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.29" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/6.3V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6602-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-220UF/25V" package="E3.5-8">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6567-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
<device name="-220UF/10V" package="E2.5-6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6565-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150UF/10V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1189-1879-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.25" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/10V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6600-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.0509" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="REG1117" prefix="LM1117">
<description>&lt;b&gt;800mA and 1A Low Dropout (LDO) Positive Regulator&lt;/b&gt;&lt;p&gt;
1.8V, 2.5V, 2.85V, 3.3V, 5V, and Adj&lt;p&gt;

&lt;b&gt;&lt;i&gt;Digikey No :-  __LM1117MPX-3.3CT-ND__</description>
<gates>
<gate name="G$1" symbol="REG1117" x="0" y="-2.54"/>
</gates>
<devices>
<device name="-3.3V" package="SOT223">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="LM1117MPX-3.3CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.06" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0.1UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1095-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FILM_AC-CAP" package="FILM-CAPACITOR-0.47U/400V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1275-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1079-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.23"/>
</technology>
</technologies>
</device>
<device name="-33PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1055-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-18PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1052-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-DNP/NI" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2087-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3022-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-3386-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.11"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1821-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.12"/>
</technology>
</technologies>
</device>
<device name="-0.01UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1132-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4700PF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2062-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1524-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3012-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/25V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3335-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7NF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9092-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-18PF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="77-VJ0805A180GXQCBC" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.06" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/10V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1356-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.24" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-9730-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1024-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/25V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1353-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3303-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1532-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22PF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9031-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-5043-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10NF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1926-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1993-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V." package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-4112-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V-0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3340-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1494-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2200PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1085-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1281-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.022UF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1063-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1296-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3V3">
<description>&lt;b&gt;SUPPLY SYMBOL</description>
<gates>
<gate name="G$1" symbol="3V3" x="0" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;Resistance&lt;/B&gt; &lt;p&gt; 
Mount :-Surface mount</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-50R_1/8W_RF" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0603-50-E3 "/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-50R_1/20W_RF" package="R-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="FC0402-50BWCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.52"/>
</technology>
</technologies>
</device>
<device name="-POWER_RESISTOR" package="10555-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-100K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-100K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-1M_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00M-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-12K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-12.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-330R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF0603JT330RCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-30R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P30.0HCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0R" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P0.0GCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-4.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-24K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM24KCFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-560R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-560GRDKR-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.7K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-2.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-33R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM33.0CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-6.2K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM6.2KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-10.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-10R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P10.0HCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5K_1/8W" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0805-5K" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300K_/1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM300KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-33K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-33.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-110K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-110KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-20K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-20.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-140K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-140KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-84.5K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-84.5KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-68.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15K_1/8W" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-15.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-47.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_0.125W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-10KARCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/4W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM1.0KDCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-698R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-698HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.50K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-3.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.58K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RC0603FR-071K58L" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-51.1R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-51.1HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.37K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.37KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-150GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-82GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-130R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-130GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12.1K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-12.1KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-270KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-100HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470AZCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.0003R/3W-CURRENTSENSE" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="HCS2512FTL300CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.9625" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-6.80ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.102" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-470ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-270KACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD-2X5" prefix="JP">
<gates>
<gate name="G$1" symbol="PINHEAD-2X5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEAD-2X5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="609-3243-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
<technology name="NHD-0220FZ-FSW-GBW-P-33V3">
<attribute name="DIGIKEY" value="NHD-0220FZ-FSW-GBW-P-33V3-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="10.9" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL" prefix="CRYSTAL" uservalue="yes">
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="-16MHZ_18PF" package="CRYSTAL-2PIN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="535-10226-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.35" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4PIN-24MHZ" package="CRYSTAL-4PIN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="535-11294-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.82" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="NX5032">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="887-1740-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.72" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8MHZ" package="CRYSTAL-4PIN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="887-1452-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.82" constant="no"/>
</technology>
</technologies>
</device>
<device name="-16MHZ" package="CRYSTAL-4PIN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="535-9122-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.68" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JUMPER-3" prefix="SJ">
<gates>
<gate name="G$1" symbol="SJ_2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJ_2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part. Plain Copper Pads." constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE">
<description>Digikey No. :- __F2906CT-ND__</description>
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1A/250V_FUSE">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="F2904CT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.45" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VARISTOR" prefix="Z">
<gates>
<gate name="G$1" symbol="VARISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-5V_VDC" package="R0603">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="CG0603MLC-05ECT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300VAC/385VDC" package="CU3225K">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="PV300K3225TCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.05" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TVS-DIODE" prefix="TVS">
<gates>
<gate name="G$1" symbol="TVS-DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="-5.8V" package="SMADIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="F5656CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.45" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5V">
<description>&lt;b&gt;SUPPLY SYMBOL</description>
<gates>
<gate name="G$1" symbol="5V" x="0" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD-1X3">
<gates>
<gate name="G$1" symbol="PINHEAD-1X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEAD-1X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="952-2264-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LS_A67K">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-" package="LS_Y876">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RED" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-2512-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.09"/>
</technology>
</technologies>
</device>
<device name="-GREEN" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-3118-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17" constant="no"/>
</technology>
</technologies>
</device>
<device name="-YELLOW" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-1196-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17"/>
</technology>
</technologies>
</device>
<device name="-BLUE" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-2816-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17"/>
</technology>
</technologies>
</device>
<device name="-5MM" package="LED-5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVR_ICSP">
<gates>
<gate name="G$1" symbol="AVR_ICSP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AVR_ICSP_PTH">
<connects>
<connect gate="G$1" pin="+V" pad="2"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RST" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="952-2121-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.23" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FERRITE-BEAD" prefix="FB" uservalue="yes">
<gates>
<gate name="G$1" symbol="FERRITE-BEAD" x="0" y="0"/>
</gates>
<devices>
<device name="-42R_100MHZ/4A" package="L0805">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1768-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-220R_100MHZ/2A" package="L0805">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="-60R_100MHZ" package="L0603">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5K_100MHZ/0.3A" package="L0603">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1553-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.044" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ISOLATED_GND">
<gates>
<gate name="G$1" symbol="ISOLATED_GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JUMPER" prefix="SJ" uservalue="yes">
<gates>
<gate name="G$1" symbol="JUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part. Plain Copper Pads." constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BOX" prefix="ENCLOSURE">
<description>Digikey Number:- __377-1890-ND__</description>
<gates>
<gate name="G$1" symbol="BOX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BOX">
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="377-1890-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="12.6" constant="no"/>
</technology>
<technology name="HAMMOND-CBOX">
<attribute name="DIGIKEY" value="HM903-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="24.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-222X146MM" package="HB1329">
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="377-1904-ND"/>
<attribute name="PRICE_PER_UNIT" value="19.40"/>
</technology>
</technologies>
</device>
<device name="-240*160MM" package="HB1340">
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="377-1925-ND &amp;  377-1628-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="25.9" constant="no"/>
</technology>
</technologies>
</device>
<device name="-92X66" package="1593BBGY">
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="HM962-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="2.78" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SCREW_CONNECTOR-2PIN" prefix="SC" uservalue="yes">
<description>&lt;b&gt;Shrouded Connector :- __ED10546-ND__ &amp; __ ED10554-ND__</description>
<gates>
<gate name="G$1" symbol="SCREW_CONNECTOR-2PIN" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="SCREW_CONNECTOR-2PIN-2.54MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-" package="SCREW_CONNECTOR-2PIN-5.08MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="ED2609-ND "/>
<attribute name="PRICE_PER_UNIT" value="0.42"/>
</technology>
</technologies>
</device>
<device name="'" package="SCREW_CONNECTOR-2PIN-3.5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RT.ANGLE" package="SHROUDED-TERMINAL-BLOCK-3.5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="ED10546-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.37" constant="no"/>
</technology>
</technologies>
</device>
<device name="-MATING_PART" package="1.27MM_HEADER-SMT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM_RT" package="SHROUDED-TERMINAL-BLOCK-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="ED2791-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.1742" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.27" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR-ARRAY-4" prefix="RN" uservalue="yes">
<description>&lt;b&gt;Resistor Array&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="R-ARRAY-4" x="0" y="10.16" addlevel="always" swaplevel="1"/>
<gate name="B" symbol="R-ARRAY-4" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="C" symbol="R-ARRAY-4" x="0" y="5.08" addlevel="always" swaplevel="1"/>
<gate name="D" symbol="R-ARRAY-4" x="0" y="2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="-10K-1/16W" package="RESISTOR-ARRAY-4-1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="742C083103JPCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-22R_1/16W" package="RESISTOR-ARRAY-4-1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="CRA6S822CT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-68K_1/16W" package="RESISTOR-ARRAY-4-1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="CAT16-683J4LFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESP07" uservalue="yes">
<gates>
<gate name="G$1" symbol="ESP07" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESP8266-07">
<connects>
<connect gate="G$1" pin="ADC" pad="ADC"/>
<connect gate="G$1" pin="CH_PD" pad="CH_PD"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GPIO0" pad="GPIO0"/>
<connect gate="G$1" pin="GPIO12" pad="GPIO12"/>
<connect gate="G$1" pin="GPIO13" pad="GPIO13"/>
<connect gate="G$1" pin="GPIO14" pad="GPIO14"/>
<connect gate="G$1" pin="GPIO15" pad="GPIO15"/>
<connect gate="G$1" pin="GPIO16" pad="GPIO16"/>
<connect gate="G$1" pin="GPIO2" pad="GPIO2"/>
<connect gate="G$1" pin="GPIO4" pad="GPIO4"/>
<connect gate="G$1" pin="GPIO5" pad="GPIO5"/>
<connect gate="G$1" pin="RESET" pad="RST"/>
<connect gate="G$1" pin="URXD" pad="RXD"/>
<connect gate="G$1" pin="UTXD" pad="TXD"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name="">
<attribute name="LINK" value="http://www.aliexpress.com/item/ESP8266-Remote-Serial-Port-WIFI-Transceiver-Wireless-Module-Esp-07-AP-STA/32356556962.html?ws_ab_test=searchweb201556_1,searchweb201527_4_71_72_73_74_75,searchweb201560_4" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="2.42" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="2.42" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RGBLED_CA">
<description>Digikey No :- __67-2125-1-ND__</description>
<gates>
<gate name="G$1" symbol="RGBLED-CA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SML-LX0404SIUPGUSB">
<connects>
<connect gate="G$1" pin="BLUE" pad="3"/>
<connect gate="G$1" pin="GREEN" pad="4"/>
<connect gate="G$1" pin="RED" pad="2"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-DIP" package="RGB-LED_5MM">
<connects>
<connect gate="G$1" pin="BLUE" pad="P$4"/>
<connect gate="G$1" pin="GREEN" pad="P$3"/>
<connect gate="G$1" pin="RED" pad="P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="LINK" value="http://www.aliexpress.com/item/Free-shipping-100pcs-5mm-RGB-LED-Common-Anode-Tri-Color-Emitting-Diodes-f5-RGB-Diffused/32429851195.html?ws_ab_test=searchweb201556_1,searchweb201527_4_71_72_73_74_75,searchweb201560_4" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.05" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TACTILE">
<description>http://www.digikey.com/product-detail/en/FSM4JSMA/450-1129-ND/525821</description>
<gates>
<gate name="G$1" symbol="TACTILE" x="0" y="0"/>
</gates>
<devices>
<device name="-SMT" package="TACTILE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="450-1129-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="-DIP" package="TACTILE-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="679-2428-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="GND-ISO">
<description>Isolated ground</description>
<pin name="GND-ISO" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="1.27" y1="2.032" x2="1.27" y2="0.508" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND-ISO" prefix="GND-ISO">
<description>Isolated ground</description>
<gates>
<gate name="G$1" symbol="GND-ISO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0.3048">
<clearance class="0" value="0.2032"/>
</class>
</classes>
<parts>
<part name="5V_AC-DC_CONVVERTOR" library="BaseApp" deviceset="HLK-PM01" device=""/>
<part name="STPM1" library="BaseApp" deviceset="STPM34" device=""/>
<part name="CPOL1" library="BaseApp" deviceset="CPOL" device="-150UF/10V" value="150UF/10V"/>
<part name="GND1" library="BaseApp" deviceset="GND" device=""/>
<part name="LM1" library="BaseApp" deviceset="REG1117" device="-3.3V"/>
<part name="C3" library="BaseApp" deviceset="CAPACITOR" device="-10UF/6.3V" value="10UF/6.3V "/>
<part name="U$3" library="BaseApp" deviceset="3V3" device=""/>
<part name="U$4" library="BaseApp" deviceset="3V3" device=""/>
<part name="C20" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value="0.1UF/6.3V"/>
<part name="CPOL3" library="BaseApp" deviceset="CPOL" device="-100UF/6.3V" value="100UF/6.3V"/>
<part name="C31" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="C29" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="C28" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="C27" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="C25" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="R7" library="BaseApp" deviceset="RESISTOR" device="-100R_1/10W" value="100R"/>
<part name="C26" library="BaseApp" deviceset="CAPACITOR" device="-10NF/16V" value="10NF"/>
<part name="U$5" library="BaseApp" deviceset="3V3" device=""/>
<part name="JP1" library="BaseApp" deviceset="PINHEAD-2X5" device=""/>
<part name="U$6" library="BaseApp" deviceset="3V3" device=""/>
<part name="C23" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value="0.1UF/6.3V"/>
<part name="C21" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="C24" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="CRYSTAL1" library="BaseApp" deviceset="CRYSTAL" device="-16MHZ" value="16MHz"/>
<part name="C34" library="BaseApp" deviceset="CAPACITOR" device="-15PF/50V" value="15PF"/>
<part name="C33" library="BaseApp" deviceset="CAPACITOR" device="-15PF/50V" value="15PF"/>
<part name="R19" library="BaseApp" deviceset="RESISTOR" device="-1M_1/8W" value="1M"/>
<part name="R26" library="BaseApp" deviceset="RESISTOR" device="-100R_1/10W"/>
<part name="C36" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150pf"/>
<part name="C37" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value="0.1uf"/>
<part name="R29" library="BaseApp" deviceset="RESISTOR" device="-22K_1/10W" value="22k"/>
<part name="SJ2" library="BaseApp" deviceset="JUMPER-3" device=""/>
<part name="U$8" library="BaseApp" deviceset="3V3" device=""/>
<part name="C17" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="C18" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="C19" library="BaseApp" deviceset="CAPACITOR" device="-150PF/50V" value="150PF"/>
<part name="C7" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value="0.1UF/6.3V"/>
<part name="C8" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value="0.1UF/6.3V"/>
<part name="C9" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value="0.1UF/6.3V"/>
<part name="C10" library="BaseApp" deviceset="CAPACITOR" device="-1UF/6.3V"/>
<part name="R14" library="BaseApp" deviceset="RESISTOR" device="-270K/0.4W" value="270K"/>
<part name="R18" library="BaseApp" deviceset="RESISTOR" device="-270K/0.4W" value="270K"/>
<part name="R21" library="BaseApp" deviceset="RESISTOR" device="-270K/0.4W" value="270K"/>
<part name="C35" library="BaseApp" deviceset="CAPACITOR" device="-22NF/50V" value="22NF"/>
<part name="R10" library="BaseApp" deviceset="RESISTOR" device="-470R/0.4W" value="470R"/>
<part name="R6" library="BaseApp" deviceset="RESISTOR" device="-270K/0.4W" value="270K"/>
<part name="R5" library="BaseApp" deviceset="RESISTOR" device="-270K/0.4W" value="270K"/>
<part name="R4" library="BaseApp" deviceset="RESISTOR" device="-270K/0.4W" value="270K"/>
<part name="C5" library="BaseApp" deviceset="CAPACITOR" device="-22NF/50V" value="22NF"/>
<part name="R9" library="BaseApp" deviceset="RESISTOR" device="-470R/0.4W" value="470R"/>
<part name="C30" library="BaseApp" deviceset="CAPACITOR" device="-10NF/16V" value="10N"/>
<part name="C11" library="BaseApp" deviceset="CAPACITOR" device="-10NF/16V" value="10N"/>
<part name="R12" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W" value="1K"/>
<part name="R24" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W" value="1K"/>
<part name="R3" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W" value="1K"/>
<part name="R1" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W" value="1K"/>
<part name="R17" library="BaseApp" deviceset="RESISTOR" device="-6.8R/0.4W" value="6R*"/>
<part name="R2" library="BaseApp" deviceset="RESISTOR" device="-6.8R/0.4W" value="6R*"/>
<part name="FUSE" library="BaseApp" deviceset="FUSE" device="" value="F2906CT-ND"/>
<part name="MOV" library="BaseApp" deviceset="VARISTOR" device="-300VAC/385VDC"/>
<part name="TVS1" library="BaseApp" deviceset="TVS-DIODE" device="-5.8V"/>
<part name="CPOL2" library="BaseApp" deviceset="CPOL" device="-220UF/10V" value="220UF/10V"/>
<part name="C1" library="BaseApp" deviceset="CAPACITOR" device="-10UF/10V"/>
<part name="C2" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/10V"/>
<part name="U$11" library="BaseApp" deviceset="5V" device=""/>
<part name="GND15" library="BaseApp" deviceset="GND" device=""/>
<part name="U$12" library="BaseApp" deviceset="5V" device=""/>
<part name="R22" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="C6" library="BaseApp" deviceset="CAPACITOR" device="-18PF/50V" value="18PF/10V"/>
<part name="C4" library="BaseApp" deviceset="CAPACITOR" device="-18PF/50V" value="18PF/10V"/>
<part name="XTAL" library="BaseApp" deviceset="CRYSTAL" device="-8MHZ" value="8MHz"/>
<part name="GND16" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R23" library="BaseApp" deviceset="RESISTOR" device="-10R_1/10W" value="10R"/>
<part name="C32" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/10V" value="0.1UF/10V"/>
<part name="C16" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/10V" value="0.1UF/10V"/>
<part name="C12" library="BaseApp" deviceset="CAPACITOR" device="-1UF/10V" value="1UF/10V"/>
<part name="ATMEGA328." library="BaseApp" deviceset="ATMEGA328P" device=""/>
<part name="AVR_ICSP" library="BaseApp" deviceset="AVR_ICSP" device=""/>
<part name="GND17" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C15" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/10V" value="0.1UF/10V"/>
<part name="R15" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W" value="330R"/>
<part name="AT-UART" library="BaseApp" deviceset="PINHEAD-1X3" device=""/>
<part name="GND39" library="BaseApp" deviceset="GND" device=""/>
<part name="U$13" library="BaseApp" deviceset="3V3" device=""/>
<part name="U$14" library="BaseApp" deviceset="3V3" device=""/>
<part name="U$15" library="BaseApp" deviceset="3V3" device=""/>
<part name="U$16" library="BaseApp" deviceset="3V3" device=""/>
<part name="C13" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V"/>
<part name="C14" library="BaseApp" deviceset="CAPACITOR" device="-1UF/6.3V"/>
<part name="GND18" library="BaseApp" deviceset="GND" device=""/>
<part name="R28" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W"/>
<part name="R27" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W"/>
<part name="R11" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W"/>
<part name="R8" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W"/>
<part name="U$17" library="BaseApp" deviceset="3V3" device=""/>
<part name="R16" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W"/>
<part name="R20" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W"/>
<part name="R13" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W"/>
<part name="FRAME1" library="frames" deviceset="A3L-LOC" device=""/>
<part name="FB6" library="BaseApp" deviceset="FERRITE-BEAD" device="-1.5K_100MHZ/0.3A"/>
<part name="FB2" library="BaseApp" deviceset="FERRITE-BEAD" device="-1.5K_100MHZ/0.3A"/>
<part name="FB1" library="BaseApp" deviceset="FERRITE-BEAD" device="-1.5K_100MHZ/0.3A"/>
<part name="FB3" library="BaseApp" deviceset="FERRITE-BEAD" device="-1.5K_100MHZ/0.3A"/>
<part name="FB4" library="BaseApp" deviceset="FERRITE-BEAD" device="-1.5K_100MHZ/0.3A"/>
<part name="FB5" library="BaseApp" deviceset="FERRITE-BEAD" device="-1.5K_100MHZ/0.3A"/>
<part name="FB7" library="BaseApp" deviceset="FERRITE-BEAD" device="-1.5K_100MHZ/0.3A"/>
<part name="U$9" library="BaseApp" deviceset="ISOLATED_GND" device=""/>
<part name="GND-ISO1" library="SparkFun-Aesthetics" deviceset="GND-ISO" device=""/>
<part name="SJ1" library="BaseApp" deviceset="JUMPER" device=""/>
<part name="C22" library="BaseApp" deviceset="CAPACITOR" device="-4.7UF/6.3V" value="4.7UF/6.3V"/>
<part name="GND-ISO2" library="SparkFun-Aesthetics" deviceset="GND-ISO" device=""/>
<part name="GND-ISO3" library="SparkFun-Aesthetics" deviceset="GND-ISO" device=""/>
<part name="GND-ISO4" library="SparkFun-Aesthetics" deviceset="GND-ISO" device=""/>
<part name="GND-ISO5" library="SparkFun-Aesthetics" deviceset="GND-ISO" device=""/>
<part name="GND-ISO6" library="SparkFun-Aesthetics" deviceset="GND-ISO" device=""/>
<part name="LED2" library="BaseApp" deviceset="LED" device="-GREEN" value="green"/>
<part name="R31" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W" value="330R"/>
<part name="LED1" library="BaseApp" deviceset="LED" device="-GREEN" value="green"/>
<part name="R30" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W" value="330R"/>
<part name="U$19" library="BaseApp" deviceset="ISOLATED_GND" device=""/>
<part name="ENCLOSURE1" library="BaseApp" deviceset="BOX" device="-92X66"/>
<part name="SC100" library="BaseApp" deviceset="SCREW_CONNECTOR-2PIN" device="-RT.ANGLE"/>
<part name="SC101" library="BaseApp" deviceset="SCREW_CONNECTOR-2PIN" device="-RT.ANGLE"/>
<part name="RN1" library="BaseApp" deviceset="RESISTOR-ARRAY-4" device="-10K-1/16W"/>
<part name="U$100" library="BaseApp" deviceset="ESP07" device=""/>
<part name="GND9" library="BaseApp" deviceset="GND" device=""/>
<part name="SC102" library="BaseApp" deviceset="SCREW_CONNECTOR-2PIN" device="-5MM_RT"/>
<part name="R25" library="BaseApp" deviceset="RESISTOR" device="-1K_1/4W"/>
<part name="U$10" library="BaseApp" deviceset="3V3" device=""/>
<part name="GND-ISO7" library="SparkFun-Aesthetics" deviceset="GND-ISO" device=""/>
<part name="U$18" library="BaseApp" deviceset="ISOLATED_GND" device=""/>
<part name="U$20" library="BaseApp" deviceset="ISOLATED_GND" device=""/>
<part name="U$21" library="BaseApp" deviceset="ISOLATED_GND" device=""/>
<part name="U$22" library="BaseApp" deviceset="ISOLATED_GND" device=""/>
<part name="U$23" library="BaseApp" deviceset="ISOLATED_GND" device=""/>
<part name="U$24" library="BaseApp" deviceset="ISOLATED_GND" device=""/>
<part name="U$2" library="BaseApp" deviceset="RGBLED_CA" device="-DIP"/>
<part name="U$7" library="BaseApp" deviceset="TACTILE" device="-DIP"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="-490.22" y1="-195.58" x2="-254" y2="-195.58" width="1.016" layer="94" style="shortdash"/>
<wire x1="-254" y1="-195.58" x2="-254" y2="-180.34" width="1.016" layer="94" style="shortdash"/>
<wire x1="-254" y1="-180.34" x2="-254" y2="-114.3" width="1.016" layer="94" style="shortdash"/>
<wire x1="-254" y1="-114.3" x2="-254" y2="7.62" width="1.016" layer="94" style="shortdash"/>
<wire x1="-254" y1="7.62" x2="-490.22" y2="7.62" width="1.016" layer="94" style="shortdash"/>
<wire x1="-490.22" y1="7.62" x2="-490.22" y2="-195.58" width="1.016" layer="94" style="shortdash"/>
<wire x1="-490.22" y1="-195.58" x2="-490.22" y2="-243.84" width="1.016" layer="94" style="shortdash"/>
<wire x1="-490.22" y1="-243.84" x2="-254" y2="-243.84" width="1.016" layer="94" style="shortdash"/>
<wire x1="-254" y1="-243.84" x2="-254" y2="-195.58" width="1.016" layer="94" style="shortdash"/>
<wire x1="-254" y1="-114.3" x2="-109.22" y2="-114.3" width="1.016" layer="94" style="shortdash"/>
<wire x1="-109.22" y1="-114.3" x2="-109.22" y2="7.62" width="1.016" layer="94" style="shortdash"/>
<wire x1="-109.22" y1="7.62" x2="-254" y2="7.62" width="1.016" layer="94" style="shortdash"/>
<wire x1="-254" y1="-180.34" x2="-109.22" y2="-180.34" width="1.016" layer="94" style="shortdash"/>
<wire x1="-109.22" y1="-180.34" x2="-109.22" y2="-114.3" width="1.016" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="5V_AC-DC_CONVVERTOR" gate="G$1" x="-386.08" y="-228.6"/>
<instance part="STPM1" gate="G$1" x="-353.06" y="-106.68"/>
<instance part="CPOL1" gate="G$1" x="-312.42" y="-228.6"/>
<instance part="GND1" gate="G$1" x="-299.72" y="-233.68"/>
<instance part="LM1" gate="G$1" x="-289.56" y="-228.6"/>
<instance part="C3" gate="G$1" x="-269.24" y="-228.6"/>
<instance part="U$3" gate="G$1" x="-269.24" y="-215.9"/>
<instance part="U$4" gate="G$1" x="-309.88" y="-73.66"/>
<instance part="C20" gate="G$1" x="-289.56" y="-83.82"/>
<instance part="CPOL3" gate="G$1" x="-304.8" y="-83.82"/>
<instance part="C31" gate="G$1" x="-411.48" y="-73.66" rot="R90"/>
<instance part="C29" gate="G$1" x="-411.48" y="-63.5" rot="R90"/>
<instance part="C28" gate="G$1" x="-411.48" y="-53.34" rot="R90"/>
<instance part="C27" gate="G$1" x="-411.48" y="-43.18" rot="R90"/>
<instance part="C25" gate="G$1" x="-411.48" y="-33.02" rot="R90"/>
<instance part="R7" gate="G$1" x="-350.52" y="-66.04" rot="R90"/>
<instance part="C26" gate="G$1" x="-411.48" y="-22.86" rot="R90"/>
<instance part="U$5" gate="G$1" x="-393.7" y="-2.54"/>
<instance part="JP1" gate="G$1" x="-353.06" y="-20.32" rot="MR270"/>
<instance part="U$6" gate="G$1" x="-335.28" y="-27.94"/>
<instance part="C23" gate="G$1" x="-312.42" y="-58.42"/>
<instance part="C21" gate="G$1" x="-274.32" y="-83.82"/>
<instance part="C24" gate="G$1" x="-325.12" y="-58.42"/>
<instance part="CRYSTAL1" gate="G$1" x="-403.86" y="-99.06" rot="R90"/>
<instance part="C34" gate="G$1" x="-421.64" y="-93.98" rot="R90"/>
<instance part="C33" gate="G$1" x="-421.64" y="-104.14" rot="R90"/>
<instance part="R19" gate="G$1" x="-411.48" y="-99.06" rot="R90"/>
<instance part="R26" gate="G$1" x="-426.72" y="-114.3"/>
<instance part="C36" gate="G$1" x="-406.4" y="-119.38"/>
<instance part="C37" gate="G$1" x="-416.56" y="-119.38"/>
<instance part="R29" gate="G$1" x="-434.34" y="-119.38" rot="R90"/>
<instance part="SJ2" gate="G$1" x="-444.5" y="-114.3" rot="R180"/>
<instance part="U$8" gate="G$1" x="-444.5" y="-104.14"/>
<instance part="C17" gate="G$1" x="-307.34" y="-121.92"/>
<instance part="C18" gate="G$1" x="-292.1" y="-114.3"/>
<instance part="C19" gate="G$1" x="-279.4" y="-109.22"/>
<instance part="C7" gate="G$1" x="-297.18" y="-121.92"/>
<instance part="C8" gate="G$1" x="-284.48" y="-116.84"/>
<instance part="C9" gate="G$1" x="-271.78" y="-109.22"/>
<instance part="C10" gate="G$1" x="-261.62" y="-109.22"/>
<instance part="R14" gate="G$1" x="-381" y="-129.54"/>
<instance part="R18" gate="G$1" x="-391.16" y="-137.16" rot="R90"/>
<instance part="R21" gate="G$1" x="-391.16" y="-149.86" rot="R90"/>
<instance part="C35" gate="G$1" x="-373.38" y="-142.24"/>
<instance part="R10" gate="G$1" x="-363.22" y="-142.24" rot="R90"/>
<instance part="R6" gate="G$1" x="-322.58" y="-129.54" rot="MR0"/>
<instance part="R5" gate="G$1" x="-312.42" y="-137.16" rot="MR90"/>
<instance part="R4" gate="G$1" x="-312.42" y="-149.86" rot="MR90"/>
<instance part="C5" gate="G$1" x="-330.2" y="-142.24" rot="MR0"/>
<instance part="R9" gate="G$1" x="-340.36" y="-142.24" rot="MR90"/>
<instance part="C30" gate="G$1" x="-355.6" y="-175.26"/>
<instance part="C11" gate="G$1" x="-347.98" y="-175.26"/>
<instance part="R12" gate="G$1" x="-368.3" y="-167.64"/>
<instance part="R24" gate="G$1" x="-368.3" y="-185.42"/>
<instance part="R3" gate="G$1" x="-335.28" y="-167.64"/>
<instance part="R1" gate="G$1" x="-335.28" y="-185.42"/>
<instance part="R17" gate="G$1" x="-375.92" y="-177.8" rot="R90"/>
<instance part="R2" gate="G$1" x="-327.66" y="-177.8" rot="R90"/>
<instance part="FUSE" gate="G$1" x="-426.72" y="-218.44" smashed="yes">
<attribute name="NAME" x="-429.26" y="-215.9" size="1.27" layer="95"/>
<attribute name="VALUE" x="-424.18" y="-220.98" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="MOV" gate="G$1" x="-414.02" y="-231.14" rot="R90"/>
<instance part="TVS1" gate="G$1" x="-322.58" y="-223.52" rot="R90"/>
<instance part="CPOL2" gate="G$1" x="-363.22" y="-228.6"/>
<instance part="C1" gate="G$1" x="-340.36" y="-228.6"/>
<instance part="C2" gate="G$1" x="-332.74" y="-228.6"/>
<instance part="U$11" gate="G$1" x="-322.58" y="-215.9"/>
<instance part="GND15" gate="G$1" x="-322.58" y="-236.22"/>
<instance part="U$12" gate="G$1" x="-312.42" y="-218.44"/>
<instance part="R22" gate="G$1" x="-231.14" y="-35.56" smashed="yes">
<attribute name="NAME" x="-233.68" y="-34.0614" size="1.1" layer="95"/>
<attribute name="VALUE" x="-228.6" y="-36.83" size="1.1" layer="95" rot="R180"/>
</instance>
<instance part="C6" gate="G$1" x="-236.22" y="-73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="-235.204" y="-76.581" size="1.1" layer="95" rot="R180"/>
<attribute name="VALUE" x="-235.204" y="-71.501" size="1.1" layer="96" rot="R180"/>
</instance>
<instance part="C4" gate="G$1" x="-228.6" y="-73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="-225.044" y="-76.581" size="1.1" layer="95" rot="R180"/>
<attribute name="VALUE" x="-225.044" y="-71.501" size="1.1" layer="96" rot="R180"/>
</instance>
<instance part="XTAL" gate="G$1" x="-233.68" y="-66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="-231.14" y="-68.326" size="1.1" layer="95" rot="R180"/>
<attribute name="VALUE" x="-231.14" y="-62.23" size="1.1" layer="96" rot="R180"/>
</instance>
<instance part="GND16" gate="1" x="-233.68" y="-96.52" smashed="yes">
<attribute name="VALUE" x="-236.22" y="-99.06" size="1.1" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="-236.22" y="-40.64" smashed="yes">
<attribute name="NAME" x="-238.76" y="-39.1414" size="1.1" layer="95"/>
<attribute name="VALUE" x="-233.68" y="-41.91" size="1.1" layer="95" rot="R180"/>
</instance>
<instance part="C32" gate="G$1" x="-241.3" y="-58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="-242.824" y="-61.341" size="1.1" layer="95" rot="R180"/>
<attribute name="VALUE" x="-242.824" y="-56.261" size="1.1" layer="96" rot="R180"/>
</instance>
<instance part="C16" gate="G$1" x="-243.84" y="-43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="-244.856" y="-42.799" size="1.1" layer="95"/>
<attribute name="VALUE" x="-237.744" y="-46.101" size="1.1" layer="96" rot="R180"/>
</instance>
<instance part="C12" gate="G$1" x="-248.92" y="-43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="-250.444" y="-46.101" size="1.1" layer="95" rot="R180"/>
<attribute name="VALUE" x="-255.016" y="-42.799" size="1.1" layer="96"/>
</instance>
<instance part="ATMEGA328." gate="G$1" x="-198.12" y="-66.04" smashed="yes">
<attribute name="NAME" x="-218.44" y="-32.258" size="1.1" layer="95"/>
<attribute name="VALUE" x="-218.44" y="-99.06" size="1.1" layer="96"/>
</instance>
<instance part="AVR_ICSP" gate="G$1" x="-203.2" y="-15.24" smashed="yes">
<attribute name="NAME" x="-212.09" y="-9.398" size="1.1" layer="95" font="vector"/>
</instance>
<instance part="GND17" gate="1" x="-190.5" y="-25.4" smashed="yes">
<attribute name="VALUE" x="-193.04" y="-27.94" size="1.1" layer="96"/>
</instance>
<instance part="C15" gate="G$1" x="-177.8" y="-12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="-179.324" y="-15.621" size="1.1" layer="95" rot="R180"/>
<attribute name="VALUE" x="-179.324" y="-10.541" size="1.1" layer="96" rot="R180"/>
</instance>
<instance part="R15" gate="G$1" x="-119.38" y="-17.78" smashed="yes" rot="MR180">
<attribute name="NAME" x="-121.92" y="-19.2786" size="1.1" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-121.92" y="-13.97" size="1.1" layer="95" rot="MR180"/>
</instance>
<instance part="AT-UART" gate="G$1" x="-119.38" y="-93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="-113.665" y="-87.63" size="1.1" layer="95" rot="R270"/>
</instance>
<instance part="GND39" gate="G$1" x="-129.54" y="-93.98" smashed="yes"/>
<instance part="U$13" gate="G$1" x="-241.3" y="-20.32"/>
<instance part="U$14" gate="G$1" x="-190.5" y="0"/>
<instance part="U$15" gate="G$1" x="-114.3" y="-15.24" rot="MR0"/>
<instance part="U$16" gate="G$1" x="-210.82" y="-152.4"/>
<instance part="C13" gate="G$1" x="-205.74" y="-162.56"/>
<instance part="C14" gate="G$1" x="-195.58" y="-162.56"/>
<instance part="GND18" gate="G$1" x="-200.66" y="-167.64" smashed="yes"/>
<instance part="R28" gate="G$1" x="-152.4" y="-58.42" rot="R180"/>
<instance part="R27" gate="G$1" x="-152.4" y="-60.96" rot="R180"/>
<instance part="R11" gate="G$1" x="-210.82" y="-121.92" rot="MR180"/>
<instance part="R8" gate="G$1" x="-210.82" y="-132.08" rot="MR180"/>
<instance part="U$17" gate="G$1" x="-220.98" y="-119.38" rot="MR0"/>
<instance part="R16" gate="G$1" x="-162.56" y="-88.9" rot="R180"/>
<instance part="R20" gate="G$1" x="-162.56" y="-91.44" rot="R180"/>
<instance part="R13" gate="G$1" x="-162.56" y="-93.98" rot="R180"/>
<instance part="FRAME1" gate="G$1" x="-492.76" y="-248.92"/>
<instance part="FB6" gate="G$1" x="-416.56" y="-154.94"/>
<instance part="FB2" gate="G$1" x="-294.64" y="-154.94"/>
<instance part="FB1" gate="G$1" x="-314.96" y="-185.42"/>
<instance part="FB3" gate="G$1" x="-314.96" y="-167.64"/>
<instance part="FB4" gate="G$1" x="-386.08" y="-167.64"/>
<instance part="FB5" gate="G$1" x="-386.08" y="-185.42"/>
<instance part="FB7" gate="G$1" x="-452.12" y="-160.02"/>
<instance part="U$9" gate="G$1" x="-444.5" y="-160.02"/>
<instance part="GND-ISO1" gate="G$1" x="-431.8" y="-162.56"/>
<instance part="SJ1" gate="G$1" x="-439.42" y="-160.02"/>
<instance part="C22" gate="G$1" x="-297.18" y="-58.42"/>
<instance part="GND-ISO2" gate="G$1" x="-408.94" y="-187.96"/>
<instance part="GND-ISO3" gate="G$1" x="-299.72" y="-187.96"/>
<instance part="GND-ISO4" gate="G$1" x="-368.3" y="-157.48"/>
<instance part="GND-ISO5" gate="G$1" x="-340.36" y="-154.94"/>
<instance part="GND-ISO6" gate="G$1" x="-317.5" y="-124.46"/>
<instance part="LED2" gate="G$1" x="-480.06" y="-132.08" smashed="yes">
<attribute name="NAME" x="-476.504" y="-134.112" size="1.1" layer="95" rot="R90"/>
<attribute name="VALUE" x="-474.345" y="-134.112" size="1.1" layer="96" rot="R90"/>
</instance>
<instance part="R31" gate="G$1" x="-480.06" y="-119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="-481.5586" y="-121.92" size="1.1" layer="95" rot="R90"/>
<attribute name="VALUE" x="-476.25" y="-121.92" size="1.1" layer="95" rot="R90"/>
</instance>
<instance part="LED1" gate="G$1" x="-472.44" y="-132.08" smashed="yes">
<attribute name="NAME" x="-468.884" y="-134.112" size="1.1" layer="95" rot="R90"/>
<attribute name="VALUE" x="-466.725" y="-134.112" size="1.1" layer="96" rot="R90"/>
</instance>
<instance part="R30" gate="G$1" x="-472.44" y="-119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="-473.9386" y="-121.92" size="1.1" layer="95" rot="R90"/>
<attribute name="VALUE" x="-468.63" y="-121.92" size="1.1" layer="95" rot="R90"/>
</instance>
<instance part="U$19" gate="G$1" x="-474.98" y="-137.16"/>
<instance part="ENCLOSURE1" gate="G$1" x="-243.84" y="-205.74"/>
<instance part="SC100" gate="G$1" x="-292.1" y="-177.8" rot="R90"/>
<instance part="SC101" gate="G$1" x="-416.56" y="-177.8" rot="MR90"/>
<instance part="RN1" gate="A" x="-403.86" y="-17.78" rot="R90"/>
<instance part="RN1" gate="B" x="-396.24" y="-17.78" rot="R90"/>
<instance part="RN1" gate="C" x="-388.62" y="-17.78" rot="R90"/>
<instance part="RN1" gate="D" x="-381" y="-17.78" rot="R90"/>
<instance part="U$100" gate="G$1" x="-175.26" y="-137.16"/>
<instance part="GND9" gate="G$1" x="-154.94" y="-167.64" smashed="yes"/>
<instance part="SC102" gate="G$1" x="-459.74" y="-223.52" rot="R270"/>
<instance part="R25" gate="G$1" x="-119.38" y="-43.18"/>
<instance part="U$10" gate="G$1" x="-114.3" y="-38.1"/>
<instance part="GND-ISO7" gate="G$1" x="-284.48" y="-129.54"/>
<instance part="U$18" gate="G$1" x="-416.56" y="-129.54"/>
<instance part="U$20" gate="G$1" x="-431.8" y="-104.14"/>
<instance part="U$21" gate="G$1" x="-426.72" y="-55.88"/>
<instance part="U$22" gate="G$1" x="-289.56" y="-91.44"/>
<instance part="U$23" gate="G$1" x="-325.12" y="-66.04"/>
<instance part="U$24" gate="G$1" x="-365.76" y="-17.78"/>
<instance part="U$2" gate="G$1" x="-137.16" y="-17.78" rot="MR0"/>
<instance part="U$7" gate="G$1" x="-137.16" y="-43.18"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="CPOL1" gate="G$1" pin="-"/>
<wire x1="-312.42" y1="-231.14" x2="-312.42" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-289.56" y1="-231.14" x2="-289.56" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-289.56" y1="-233.68" x2="-299.72" y2="-233.68" width="0.1524" layer="91"/>
<pinref part="GND1" gate="G$1" pin="GND"/>
<wire x1="-299.72" y1="-233.68" x2="-312.42" y2="-233.68" width="0.1524" layer="91"/>
<junction x="-299.72" y="-233.68"/>
<pinref part="LM1" gate="G$1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-289.56" y1="-233.68" x2="-269.24" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-269.24" y1="-233.68" x2="-269.24" y2="-231.14" width="0.1524" layer="91"/>
<junction x="-289.56" y="-233.68"/>
</segment>
<segment>
<pinref part="5V_AC-DC_CONVVERTOR" gate="G$1" pin="-VO"/>
<pinref part="CPOL2" gate="G$1" pin="-"/>
<wire x1="-370.84" y1="-233.68" x2="-363.22" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-363.22" y1="-233.68" x2="-363.22" y2="-231.14" width="0.1524" layer="91"/>
<junction x="-363.22" y="-233.68"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-363.22" y1="-233.68" x2="-340.36" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="-233.68" x2="-340.36" y2="-231.14" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-340.36" y1="-233.68" x2="-332.74" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-332.74" y1="-233.68" x2="-332.74" y2="-231.14" width="0.1524" layer="91"/>
<junction x="-340.36" y="-233.68"/>
<pinref part="TVS1" gate="G$1" pin="A"/>
<wire x1="-332.74" y1="-233.68" x2="-322.58" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-322.58" y1="-233.68" x2="-322.58" y2="-231.14" width="0.1524" layer="91"/>
<junction x="-332.74" y="-233.68"/>
<wire x1="-322.58" y1="-233.68" x2="-322.58" y2="-236.22" width="0.1524" layer="91"/>
<junction x="-322.58" y="-233.68"/>
<pinref part="GND15" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="-223.52" y1="-91.44" x2="-233.68" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-91.44" x2="-233.68" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="-88.9" x2="-233.68" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-88.9" x2="-233.68" y2="-91.44" width="0.1524" layer="91"/>
<junction x="-233.68" y="-91.44"/>
<wire x1="-223.52" y1="-86.36" x2="-233.68" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-86.36" x2="-233.68" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-233.68" y="-88.9"/>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-236.22" y1="-78.74" x2="-233.68" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-78.74" x2="-228.6" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-78.74" x2="-233.68" y2="-86.36" width="0.1524" layer="91"/>
<junction x="-233.68" y="-78.74"/>
<junction x="-233.68" y="-86.36"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="-241.3" y1="-63.5" x2="-241.3" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="-93.98" x2="-233.68" y2="-93.98" width="0.1524" layer="91"/>
<junction x="-233.68" y="-93.98"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="-243.84" y1="-48.26" x2="-243.84" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="-93.98" x2="-241.3" y2="-93.98" width="0.1524" layer="91"/>
<junction x="-241.3" y="-93.98"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="-248.92" y1="-48.26" x2="-243.84" y2="-48.26" width="0.1524" layer="91"/>
<junction x="-243.84" y="-48.26"/>
<pinref part="ATMEGA328." gate="G$1" pin="GND@3"/>
<pinref part="ATMEGA328." gate="G$1" pin="GND@5"/>
<pinref part="ATMEGA328." gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="AVR_ICSP" gate="G$1" pin="GND"/>
<wire x1="-195.58" y1="-17.78" x2="-190.5" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="-17.78" x2="-190.5" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-190.5" y1="-22.86" x2="-177.8" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="-22.86" x2="-177.8" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-190.5" y="-22.86"/>
</segment>
<segment>
<pinref part="AT-UART" gate="G$1" pin="3"/>
<wire x1="-121.92" y1="-91.44" x2="-129.54" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="-129.54" y1="-91.44" x2="-129.54" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="GND39" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-205.74" y1="-165.1" x2="-205.74" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="-167.64" x2="-200.66" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="-200.66" y1="-167.64" x2="-195.58" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="-167.64" x2="-195.58" y2="-165.1" width="0.1524" layer="91"/>
<pinref part="GND18" gate="G$1" pin="GND"/>
<junction x="-200.66" y="-167.64"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="GND"/>
<pinref part="GND9" gate="G$1" pin="GND"/>
<wire x1="-157.48" y1="-157.48" x2="-154.94" y2="-157.48" width="0.1524" layer="91"/>
<wire x1="-154.94" y1="-157.48" x2="-154.94" y2="-167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="LM1" gate="G$1" pin="VOUT"/>
<wire x1="-274.32" y1="-220.98" x2="-269.24" y2="-220.98" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-269.24" y1="-223.52" x2="-269.24" y2="-220.98" width="0.1524" layer="91"/>
<junction x="-269.24" y="-220.98"/>
<pinref part="U$3" gate="G$1" pin="+3V3"/>
<wire x1="-269.24" y1="-220.98" x2="-269.24" y2="-215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="STPM1" gate="G$1" pin="VCC"/>
<wire x1="-322.58" y1="-99.06" x2="-309.88" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-309.88" y1="-99.06" x2="-309.88" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="+3V3"/>
<pinref part="CPOL3" gate="G$1" pin="+"/>
<wire x1="-309.88" y1="-78.74" x2="-309.88" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-309.88" y1="-78.74" x2="-304.8" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-309.88" y="-78.74"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="-304.8" y1="-78.74" x2="-289.56" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-304.8" y="-78.74"/>
<wire x1="-289.56" y1="-78.74" x2="-274.32" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-289.56" y="-78.74"/>
<pinref part="C21" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-403.86" y1="-12.7" x2="-403.86" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-403.86" y1="-10.16" x2="-396.24" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-396.24" y1="-10.16" x2="-393.7" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-393.7" y1="-10.16" x2="-388.62" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-388.62" y1="-10.16" x2="-381" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-381" y1="-10.16" x2="-381" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-388.62" y1="-12.7" x2="-388.62" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-388.62" y="-10.16"/>
<wire x1="-396.24" y1="-12.7" x2="-396.24" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-396.24" y="-10.16"/>
<pinref part="U$5" gate="G$1" pin="+3V3"/>
<wire x1="-393.7" y1="-10.16" x2="-393.7" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-393.7" y="-10.16"/>
<pinref part="RN1" gate="A" pin="2"/>
<pinref part="RN1" gate="B" pin="2"/>
<pinref part="RN1" gate="C" pin="2"/>
<pinref part="RN1" gate="D" pin="2"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="10"/>
<wire x1="-347.98" y1="-25.4" x2="-347.98" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="-33.02" x2="-335.28" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-335.28" y1="-33.02" x2="-335.28" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="SJ2" gate="G$1" pin="3"/>
<wire x1="-444.5" y1="-109.22" x2="-444.5" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="-241.3" y1="-43.18" x2="-241.3" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="-40.64" x2="-241.3" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="-35.56" x2="-241.3" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="-43.18" x2="-241.3" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="-45.72" x2="-241.3" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="-45.72" x2="-241.3" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-241.3" y="-43.18"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="-236.22" y1="-35.56" x2="-241.3" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-241.3" y="-35.56"/>
<pinref part="R23" gate="G$1" pin="1"/>
<junction x="-241.3" y="-40.64"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="-243.84" y1="-40.64" x2="-241.3" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="-248.92" y1="-40.64" x2="-243.84" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-243.84" y="-40.64"/>
<pinref part="ATMEGA328." gate="G$1" pin="VCC@4"/>
<pinref part="ATMEGA328." gate="G$1" pin="VCC@6"/>
<pinref part="U$13" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="AVR_ICSP" gate="G$1" pin="+V"/>
<wire x1="-195.58" y1="-12.7" x2="-190.5" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="-12.7" x2="-190.5" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="-190.5" y1="-7.62" x2="-190.5" y2="0" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="-7.62" x2="-177.8" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="-7.62" x2="-177.8" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-190.5" y="-7.62"/>
<pinref part="U$14" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-114.3" y1="-17.78" x2="-114.3" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U$16" gate="G$1" pin="+3V3"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-210.82" y1="-157.48" x2="-210.82" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-157.48" x2="-205.74" y2="-157.48" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-205.74" y1="-157.48" x2="-195.58" y2="-157.48" width="0.1524" layer="91"/>
<junction x="-205.74" y="-157.48"/>
<pinref part="U$100" gate="G$1" pin="VCC"/>
<wire x1="-195.58" y1="-157.48" x2="-193.04" y2="-157.48" width="0.1524" layer="91"/>
<junction x="-195.58" y="-157.48"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-215.9" y1="-132.08" x2="-220.98" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="-132.08" x2="-220.98" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="-220.98" y1="-121.92" x2="-220.98" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-121.92" x2="-220.98" y2="-121.92" width="0.1524" layer="91"/>
<junction x="-220.98" y="-121.92"/>
<pinref part="U$17" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="-114.3" y1="-43.18" x2="-114.3" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="STPM_MISO" class="0">
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="-408.94" y1="-73.66" x2="-403.86" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-403.86" y1="-22.86" x2="-403.86" y2="-73.66" width="0.1524" layer="91"/>
<junction x="-403.86" y="-73.66"/>
<pinref part="STPM1" gate="G$1" pin="MISO/TXD"/>
<wire x1="-403.86" y1="-73.66" x2="-360.68" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-360.68" y1="-73.66" x2="-360.68" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="-360.68" y1="-73.66" x2="-360.68" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-360.68" y="-73.66"/>
<pinref part="JP1" gate="G$1" pin="4"/>
<wire x1="-360.68" y1="-38.1" x2="-355.6" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="-38.1" x2="-355.6" y2="-25.4" width="0.1524" layer="91"/>
<label x="-360.68" y="-81.28" size="1.778" layer="95" rot="R90"/>
<pinref part="RN1" gate="A" pin="1"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="-157.48" y1="-91.44" x2="-149.86" y2="-91.44" width="0.1524" layer="91"/>
<label x="-157.48" y="-91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="STPM_SCLK" class="0">
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="-408.94" y1="-53.34" x2="-388.62" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-388.62" y1="-22.86" x2="-388.62" y2="-53.34" width="0.1524" layer="91"/>
<junction x="-388.62" y="-53.34"/>
<pinref part="STPM1" gate="G$1" pin="SCL"/>
<wire x1="-388.62" y1="-53.34" x2="-355.6" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="-53.34" x2="-355.6" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="-53.34" x2="-355.6" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="-40.64" x2="-353.06" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-355.6" y="-53.34"/>
<pinref part="JP1" gate="G$1" pin="6"/>
<wire x1="-353.06" y1="-40.64" x2="-353.06" y2="-25.4" width="0.1524" layer="91"/>
<label x="-355.6" y="-81.28" size="1.778" layer="95" rot="R90"/>
<pinref part="RN1" gate="C" pin="1"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="-157.48" y1="-93.98" x2="-149.86" y2="-93.98" width="0.1524" layer="91"/>
<label x="-157.48" y="-93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="STPM_CS" class="0">
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="-408.94" y1="-43.18" x2="-373.38" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="STPM1" gate="G$1" pin="SCS"/>
<wire x1="-373.38" y1="-43.18" x2="-353.06" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-353.06" y1="-43.18" x2="-353.06" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-43.18" x2="-373.38" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-7.62" x2="-353.06" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-373.38" y="-43.18"/>
<pinref part="JP1" gate="G$1" pin="5"/>
<wire x1="-353.06" y1="-7.62" x2="-353.06" y2="-17.78" width="0.1524" layer="91"/>
<label x="-353.06" y="-81.28" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PB2(SS/OC1B)"/>
<wire x1="-175.26" y1="-86.36" x2="-167.64" y2="-86.36" width="0.1524" layer="91"/>
<label x="-172.72" y="-86.36" size="1.016" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="-408.94" y1="-33.02" x2="-381" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-381" y1="-33.02" x2="-350.52" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-350.52" y1="-33.02" x2="-350.52" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-381" y1="-22.86" x2="-381" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-381" y="-33.02"/>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="-408.94" y1="-22.86" x2="-408.94" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-408.94" y="-33.02"/>
<pinref part="JP1" gate="G$1" pin="8"/>
<wire x1="-350.52" y1="-25.4" x2="-350.52" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-350.52" y="-33.02"/>
<pinref part="RN1" gate="D" pin="1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="SYN"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-350.52" y1="-71.12" x2="-350.52" y2="-83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STPM_MOSI" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="MOSI/RXD"/>
<wire x1="-396.24" y1="-63.5" x2="-358.14" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-358.14" y1="-63.5" x2="-358.14" y2="-83.82" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="-408.94" y1="-63.5" x2="-396.24" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-396.24" y1="-22.86" x2="-396.24" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-396.24" y="-63.5"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="-358.14" y1="-63.5" x2="-358.14" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-358.14" y="-63.5"/>
<label x="-358.14" y="-81.28" size="1.778" layer="95" rot="R90"/>
<pinref part="RN1" gate="B" pin="1"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-157.48" y1="-88.9" x2="-149.86" y2="-88.9" width="0.1524" layer="91"/>
<label x="-157.48" y="-88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="XTAL1"/>
<pinref part="CRYSTAL1" gate="G$1" pin="1"/>
<wire x1="-383.54" y1="-101.6" x2="-403.86" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="-403.86" y1="-101.6" x2="-403.86" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="-403.86" y1="-104.14" x2="-411.48" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-403.86" y="-101.6"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="-411.48" y1="-104.14" x2="-419.1" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-411.48" y="-104.14"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="CLKIN/XTAL2"/>
<wire x1="-383.54" y1="-99.06" x2="-398.78" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-398.78" y1="-99.06" x2="-398.78" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="CRYSTAL1" gate="G$1" pin="2"/>
<wire x1="-398.78" y1="-96.52" x2="-403.86" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="-403.86" y1="-96.52" x2="-403.86" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-403.86" y1="-93.98" x2="-411.48" y2="-93.98" width="0.1524" layer="91"/>
<junction x="-403.86" y="-96.52"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="-411.48" y1="-93.98" x2="-419.1" y2="-93.98" width="0.1524" layer="91"/>
<junction x="-411.48" y="-93.98"/>
</segment>
</net>
<net name="STPM_INT1" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="INT1"/>
<wire x1="-383.54" y1="-109.22" x2="-401.32" y2="-109.22" width="0.1524" layer="91"/>
<label x="-401.32" y="-109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PD2(INT0)"/>
<wire x1="-175.26" y1="-63.5" x2="-165.1" y2="-63.5" width="0.1524" layer="91"/>
<label x="-172.72" y="-63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="STPM_INT2" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="INT2"/>
<wire x1="-383.54" y1="-111.76" x2="-401.32" y2="-111.76" width="0.1524" layer="91"/>
<label x="-401.32" y="-111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PD3(INT1)"/>
<wire x1="-175.26" y1="-66.04" x2="-165.1" y2="-66.04" width="0.1524" layer="91"/>
<label x="-172.72" y="-66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="STPM_LED2" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="LED2"/>
<wire x1="-383.54" y1="-106.68" x2="-401.32" y2="-106.68" width="0.1524" layer="91"/>
<label x="-401.32" y="-106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PC5(ADC5/SCL)"/>
<wire x1="-175.26" y1="-48.26" x2="-162.56" y2="-48.26" width="0.1524" layer="91"/>
<label x="-172.72" y="-48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="-472.44" y1="-114.3" x2="-472.44" y2="-106.68" width="0.1524" layer="91"/>
<label x="-472.44" y="-114.3" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="STPM_EN/RST" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="-421.64" y1="-114.3" x2="-416.56" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="-416.56" y1="-114.3" x2="-406.4" y2="-114.3" width="0.1524" layer="91"/>
<junction x="-416.56" y="-114.3"/>
<junction x="-406.4" y="-114.3"/>
<pinref part="STPM1" gate="G$1" pin="EN"/>
<wire x1="-383.54" y1="-114.3" x2="-406.4" y2="-114.3" width="0.1524" layer="91"/>
<label x="-401.32" y="-114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PD4(XCK/T0)"/>
<wire x1="-175.26" y1="-68.58" x2="-165.1" y2="-68.58" width="0.1524" layer="91"/>
<label x="-172.72" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="STPM_LED1" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="LED1"/>
<wire x1="-383.54" y1="-104.14" x2="-401.32" y2="-104.14" width="0.1524" layer="91"/>
<label x="-401.32" y="-104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PC4(ADC4/SDA)"/>
<wire x1="-175.26" y1="-45.72" x2="-162.56" y2="-45.72" width="0.1524" layer="91"/>
<label x="-172.72" y="-45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="-480.06" y1="-114.3" x2="-480.06" y2="-106.68" width="0.1524" layer="91"/>
<label x="-480.06" y="-114.3" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CKOUT/ZCR" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="CLKOUT/ZCR"/>
<wire x1="-383.54" y1="-96.52" x2="-393.7" y2="-96.52" width="0.1524" layer="91"/>
<label x="-396.24" y="-96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="-434.34" y1="-114.3" x2="-431.8" y2="-114.3" width="0.1524" layer="91"/>
<junction x="-434.34" y="-114.3"/>
<pinref part="SJ2" gate="G$1" pin="2"/>
<wire x1="-439.42" y1="-114.3" x2="-434.34" y2="-114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="VREF1"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="-322.58" y1="-114.3" x2="-307.34" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-307.34" y1="-114.3" x2="-307.34" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-307.34" y1="-114.3" x2="-297.18" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-297.18" y1="-114.3" x2="-297.18" y2="-116.84" width="0.1524" layer="91"/>
<junction x="-307.34" y="-114.3"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="VREF2"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="-322.58" y1="-109.22" x2="-292.1" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="-292.1" y1="-109.22" x2="-284.48" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="-284.48" y1="-109.22" x2="-284.48" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-292.1" y="-109.22"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="VDDA"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="-322.58" y1="-104.14" x2="-279.4" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-279.4" y1="-104.14" x2="-271.78" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-279.4" y="-104.14"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="-104.14" x2="-261.62" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-271.78" y="-104.14"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="STPM1" gate="G$1" pin="VIP1"/>
<wire x1="-375.92" y1="-129.54" x2="-373.38" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-129.54" x2="-363.22" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-363.22" y1="-129.54" x2="-360.68" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-360.68" y1="-129.54" x2="-360.68" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="-373.38" y1="-137.16" x2="-373.38" y2="-129.54" width="0.1524" layer="91"/>
<junction x="-373.38" y="-129.54"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="-363.22" y1="-137.16" x2="-363.22" y2="-129.54" width="0.1524" layer="91"/>
<junction x="-363.22" y="-129.54"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="-386.08" y1="-129.54" x2="-391.16" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-391.16" y1="-129.54" x2="-391.16" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="-391.16" y1="-144.78" x2="-391.16" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="-411.48" y1="-154.94" x2="-391.16" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="FB6" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-327.66" y1="-129.54" x2="-330.2" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-330.2" y1="-129.54" x2="-340.36" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="-129.54" x2="-342.9" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="-129.54" x2="-342.9" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-330.2" y1="-137.16" x2="-330.2" y2="-129.54" width="0.1524" layer="91"/>
<junction x="-330.2" y="-129.54"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="-340.36" y1="-137.16" x2="-340.36" y2="-129.54" width="0.1524" layer="91"/>
<junction x="-340.36" y="-129.54"/>
<pinref part="STPM1" gate="G$1" pin="VIP2"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-317.5" y1="-129.54" x2="-312.42" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="-129.54" x2="-312.42" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-312.42" y1="-144.78" x2="-312.42" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-299.72" y1="-154.94" x2="-312.42" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="FB2" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="IIP1"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="-355.6" y1="-124.46" x2="-355.6" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="-355.6" y1="-167.64" x2="-355.6" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="-363.22" y1="-167.64" x2="-355.6" y2="-167.64" width="0.1524" layer="91"/>
<junction x="-355.6" y="-167.64"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="IIN2"/>
<wire x1="-350.52" y1="-124.46" x2="-350.52" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-347.98" y1="-177.8" x2="-347.98" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="-185.42" x2="-340.36" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-350.52" y1="-185.42" x2="-347.98" y2="-185.42" width="0.1524" layer="91"/>
<junction x="-347.98" y="-185.42"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="STPM1" gate="G$1" pin="IIP2"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-347.98" y1="-124.46" x2="-347.98" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-347.98" y1="-167.64" x2="-347.98" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="-167.64" x2="-340.36" y2="-167.64" width="0.1524" layer="91"/>
<junction x="-347.98" y="-167.64"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="-363.22" y1="-185.42" x2="-355.6" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="-185.42" x2="-355.6" y2="-177.8" width="0.1524" layer="91"/>
<pinref part="STPM1" gate="G$1" pin="IIN1"/>
<wire x1="-353.06" y1="-124.46" x2="-353.06" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="-185.42" x2="-353.06" y2="-185.42" width="0.1524" layer="91"/>
<junction x="-355.6" y="-185.42"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="-373.38" y1="-167.64" x2="-375.92" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-375.92" y1="-167.64" x2="-375.92" y2="-172.72" width="0.1524" layer="91"/>
<wire x1="-381" y1="-167.64" x2="-375.92" y2="-167.64" width="0.1524" layer="91"/>
<junction x="-375.92" y="-167.64"/>
<pinref part="FB4" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="-375.92" y1="-182.88" x2="-375.92" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-375.92" y1="-185.42" x2="-373.38" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-381" y1="-185.42" x2="-375.92" y2="-185.42" width="0.1524" layer="91"/>
<junction x="-375.92" y="-185.42"/>
<pinref part="FB5" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-330.2" y1="-167.64" x2="-327.66" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-327.66" y1="-167.64" x2="-327.66" y2="-172.72" width="0.1524" layer="91"/>
<wire x1="-327.66" y1="-167.64" x2="-320.04" y2="-167.64" width="0.1524" layer="91"/>
<junction x="-327.66" y="-167.64"/>
<pinref part="FB3" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-330.2" y1="-185.42" x2="-327.66" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-327.66" y1="-185.42" x2="-327.66" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="-327.66" y1="-185.42" x2="-320.04" y2="-185.42" width="0.1524" layer="91"/>
<junction x="-327.66" y="-185.42"/>
<pinref part="FB1" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="-309.88" y1="-167.64" x2="-299.72" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-299.72" y1="-167.64" x2="-299.72" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="FB3" gate="G$1" pin="P$1"/>
<pinref part="SC100" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<wire x1="-391.16" y1="-167.64" x2="-408.94" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-408.94" y1="-167.64" x2="-408.94" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="FB4" gate="G$1" pin="P$2"/>
<pinref part="SC101" gate="G$1" pin="2"/>
</segment>
</net>
<net name="NEUTRAL" class="0">
<segment>
<pinref part="MOV" gate="G$1" pin="P$1"/>
<wire x1="-414.02" y1="-236.22" x2="-401.32" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="-414.02" y1="-236.22" x2="-449.58" y2="-236.22" width="0.1524" layer="91"/>
<junction x="-414.02" y="-236.22"/>
<pinref part="5V_AC-DC_CONVVERTOR" gate="G$1" pin="N"/>
<wire x1="-401.32" y1="-236.22" x2="-401.32" y2="-233.68" width="0.1524" layer="91"/>
<label x="-441.96" y="-236.22" size="1.778" layer="95"/>
<pinref part="SC102" gate="G$1" pin="2"/>
<wire x1="-452.12" y1="-228.6" x2="-449.58" y2="-228.6" width="0.1524" layer="91"/>
<wire x1="-449.58" y1="-228.6" x2="-449.58" y2="-236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="FB7" gate="G$1" pin="P$2"/>
<wire x1="-457.2" y1="-160.02" x2="-462.28" y2="-160.02" width="0.1524" layer="91"/>
<label x="-462.28" y="-160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="FUSE" gate="G$1" pin="P$2"/>
<wire x1="-401.32" y1="-218.44" x2="-414.02" y2="-218.44" width="0.1524" layer="91"/>
<wire x1="-414.02" y1="-218.44" x2="-414.02" y2="-226.06" width="0.1524" layer="91"/>
<wire x1="-416.56" y1="-218.44" x2="-414.02" y2="-218.44" width="0.1524" layer="91"/>
<junction x="-414.02" y="-218.44"/>
<pinref part="MOV" gate="G$1" pin="P$2"/>
<pinref part="5V_AC-DC_CONVVERTOR" gate="G$1" pin="L"/>
<wire x1="-401.32" y1="-218.44" x2="-401.32" y2="-223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LINE" class="0">
<segment>
<pinref part="FUSE" gate="G$1" pin="P$1"/>
<label x="-441.96" y="-218.44" size="1.778" layer="95"/>
<wire x1="-449.58" y1="-218.44" x2="-436.88" y2="-218.44" width="0.1524" layer="91"/>
<pinref part="SC102" gate="G$1" pin="1"/>
<wire x1="-452.12" y1="-223.52" x2="-449.58" y2="-223.52" width="0.1524" layer="91"/>
<wire x1="-449.58" y1="-223.52" x2="-449.58" y2="-218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-421.64" y1="-154.94" x2="-436.88" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="-436.88" y1="-154.94" x2="-436.88" y2="-152.4" width="0.1524" layer="91"/>
<label x="-436.88" y="-152.4" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="FB6" gate="G$1" pin="P$2"/>
</segment>
<segment>
<wire x1="-289.56" y1="-154.94" x2="-274.32" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="-274.32" y1="-154.94" x2="-274.32" y2="-152.4" width="0.1524" layer="91"/>
<label x="-274.32" y="-152.4" size="1.778" layer="95" xref="yes"/>
<pinref part="FB2" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="5V_AC-DC_CONVVERTOR" gate="G$1" pin="+VO"/>
<pinref part="CPOL2" gate="G$1" pin="+"/>
<wire x1="-370.84" y1="-223.52" x2="-363.22" y2="-223.52" width="0.1524" layer="91"/>
<junction x="-363.22" y="-223.52"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-363.22" y1="-223.52" x2="-340.36" y2="-223.52" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-340.36" y1="-223.52" x2="-332.74" y2="-223.52" width="0.1524" layer="91"/>
<junction x="-340.36" y="-223.52"/>
<pinref part="TVS1" gate="G$1" pin="C"/>
<wire x1="-332.74" y1="-223.52" x2="-332.74" y2="-220.98" width="0.1524" layer="91"/>
<wire x1="-332.74" y1="-220.98" x2="-322.58" y2="-220.98" width="0.1524" layer="91"/>
<junction x="-332.74" y="-223.52"/>
<wire x1="-322.58" y1="-220.98" x2="-322.58" y2="-215.9" width="0.1524" layer="91"/>
<junction x="-322.58" y="-220.98"/>
<pinref part="U$11" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="CPOL1" gate="G$1" pin="+"/>
<wire x1="-312.42" y1="-223.52" x2="-312.42" y2="-220.98" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="-220.98" x2="-304.8" y2="-220.98" width="0.1524" layer="91"/>
<pinref part="LM1" gate="G$1" pin="VIN"/>
<pinref part="U$12" gate="G$1" pin="+5V"/>
<wire x1="-312.42" y1="-220.98" x2="-312.42" y2="-218.44" width="0.1524" layer="91"/>
<junction x="-312.42" y="-220.98"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="XTAL" gate="G$1" pin="1"/>
<wire x1="-223.52" y1="-66.04" x2="-228.6" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="-228.6" y1="-66.04" x2="-231.14" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-228.6" y1="-66.04" x2="-228.6" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-228.6" y="-66.04"/>
<pinref part="ATMEGA328." gate="G$1" pin="PB7(XTAL2/TOSC2)"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="XTAL" gate="G$1" pin="2"/>
<wire x1="-223.52" y1="-60.96" x2="-236.22" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="-60.96" x2="-236.22" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-236.22" y1="-66.04" x2="-236.22" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-236.22" y="-66.04"/>
<pinref part="ATMEGA328." gate="G$1" pin="PB6(XTAL1/TOSC1)"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="-231.14" y1="-40.64" x2="-223.52" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="ATMEGA328." gate="G$1" pin="AVCC"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="-226.06" y1="-50.8" x2="-223.52" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-226.06" y1="-50.8" x2="-226.06" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-226.06" y1="-55.88" x2="-241.3" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="ATMEGA328." gate="G$1" pin="AREF"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="AVR_ICSP" gate="G$1" pin="RST"/>
<wire x1="-210.82" y1="-17.78" x2="-220.98" y2="-17.78" width="0.1524" layer="91"/>
<label x="-220.98" y="-17.78" size="1.1" layer="95"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="-226.06" y1="-35.56" x2="-223.52" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="ATMEGA328." gate="G$1" pin="PC6(/RESET)"/>
<wire x1="-226.06" y1="-35.56" x2="-226.06" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-226.06" y="-35.56"/>
<label x="-226.06" y="-33.02" size="1.1" layer="95" rot="R90"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PB5(SCK)"/>
<wire x1="-175.26" y1="-93.98" x2="-167.64" y2="-93.98" width="0.1524" layer="91"/>
<label x="-172.72" y="-93.98" size="1.1" layer="95"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AVR_ICSP" gate="G$1" pin="SCK"/>
<wire x1="-210.82" y1="-15.24" x2="-220.98" y2="-15.24" width="0.1524" layer="91"/>
<label x="-220.98" y="-15.24" size="1.1" layer="95"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PB4(MISO)"/>
<wire x1="-175.26" y1="-91.44" x2="-167.64" y2="-91.44" width="0.1524" layer="91"/>
<label x="-172.72" y="-91.44" size="1.1" layer="95"/>
<pinref part="R20" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AVR_ICSP" gate="G$1" pin="MISO"/>
<wire x1="-210.82" y1="-12.7" x2="-220.98" y2="-12.7" width="0.1524" layer="91"/>
<label x="-220.98" y="-12.7" size="1.1" layer="95"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PB3(MOSI/OC2)"/>
<wire x1="-175.26" y1="-88.9" x2="-167.64" y2="-88.9" width="0.1524" layer="91"/>
<label x="-172.72" y="-88.9" size="1.1" layer="95"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AVR_ICSP" gate="G$1" pin="MOSI"/>
<wire x1="-195.58" y1="-15.24" x2="-185.42" y2="-15.24" width="0.1524" layer="91"/>
<label x="-190.5" y="-15.24" size="1.1" layer="95"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-127" y1="-17.78" x2="-124.46" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="IO-LED" class="0">
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PC3(ADC3)"/>
<wire x1="-175.26" y1="-43.18" x2="-144.78" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="1"/>
<wire x1="-144.78" y1="-43.18" x2="-142.24" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-144.78" y="-43.18"/>
<pinref part="U$7" gate="G$1" pin="2"/>
<wire x1="-144.78" y1="-43.18" x2="-144.78" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ESP-RST" class="0">
<segment>
<wire x1="-193.04" y1="-121.92" x2="-205.74" y2="-121.92" width="0.1524" layer="91"/>
<label x="-195.58" y="-121.92" size="1.778" layer="95" rot="MR0"/>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="U$100" gate="G$1" pin="RESET"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PD7(AIN1)"/>
<wire x1="-175.26" y1="-76.2" x2="-165.1" y2="-76.2" width="0.1524" layer="91"/>
<label x="-172.72" y="-76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="ESP-TX" class="0">
<segment>
<wire x1="-157.48" y1="-121.92" x2="-144.78" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="UTXD"/>
<label x="-154.94" y="-121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="-147.32" y1="-58.42" x2="-139.7" y2="-58.42" width="0.1524" layer="91"/>
<label x="-147.32" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="ESP-RX" class="0">
<segment>
<wire x1="-144.78" y1="-127" x2="-157.48" y2="-127" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="URXD"/>
<label x="-154.94" y="-127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="-147.32" y1="-60.96" x2="-139.7" y2="-60.96" width="0.1524" layer="91"/>
<label x="-147.32" y="-60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="CH_PD" class="0">
<segment>
<wire x1="-193.04" y1="-132.08" x2="-205.74" y2="-132.08" width="0.1524" layer="91"/>
<label x="-198.12" y="-132.08" size="1.778" layer="95" rot="MR0"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="U$100" gate="G$1" pin="CH_PD"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PD6(AIN0)"/>
<wire x1="-175.26" y1="-73.66" x2="-165.1" y2="-73.66" width="0.1524" layer="91"/>
<label x="-172.72" y="-73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PD0(RXD)"/>
<wire x1="-175.26" y1="-58.42" x2="-157.48" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
<label x="-172.72" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AT-UART" gate="G$1" pin="2"/>
<wire x1="-119.38" y1="-91.44" x2="-119.38" y2="-78.74" width="0.1524" layer="91"/>
<label x="-119.38" y="-83.82" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PD1(TXD)"/>
<wire x1="-175.26" y1="-60.96" x2="-157.48" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<label x="-172.72" y="-60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AT-UART" gate="G$1" pin="1"/>
<wire x1="-116.84" y1="-91.44" x2="-116.84" y2="-78.74" width="0.1524" layer="91"/>
<label x="-116.84" y="-83.82" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="FB7" gate="G$1" pin="P$1"/>
<pinref part="U$9" gate="G$1" pin="AGND"/>
<wire x1="-447.04" y1="-160.02" x2="-444.5" y2="-160.02" width="0.1524" layer="91"/>
<pinref part="SJ1" gate="G$1" pin="1"/>
<junction x="-444.5" y="-160.02"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="-480.06" y1="-134.62" x2="-480.06" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="-480.06" y1="-137.16" x2="-474.98" y2="-137.16" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="-474.98" y1="-137.16" x2="-472.44" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="-472.44" y1="-137.16" x2="-472.44" y2="-134.62" width="0.1524" layer="91"/>
<pinref part="U$19" gate="G$1" pin="AGND"/>
<junction x="-474.98" y="-137.16"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="-434.34" y1="-124.46" x2="-416.56" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="-416.56" y1="-124.46" x2="-416.56" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="-416.56" y1="-124.46" x2="-406.4" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="-406.4" y1="-124.46" x2="-406.4" y2="-121.92" width="0.1524" layer="91"/>
<junction x="-416.56" y="-124.46"/>
<wire x1="-416.56" y1="-124.46" x2="-416.56" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="SJ2" gate="G$1" pin="1"/>
<wire x1="-444.5" y1="-119.38" x2="-444.5" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="-444.5" y1="-124.46" x2="-434.34" y2="-124.46" width="0.1524" layer="91"/>
<junction x="-434.34" y="-124.46"/>
<pinref part="U$18" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="-426.72" y1="-93.98" x2="-426.72" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-426.72" y1="-99.06" x2="-426.72" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="-426.72" y1="-99.06" x2="-431.8" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-431.8" y1="-99.06" x2="-431.8" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-426.72" y="-99.06"/>
<pinref part="U$20" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="-416.56" y1="-22.86" x2="-416.56" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="-416.56" y1="-33.02" x2="-416.56" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-416.56" y="-33.02"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="-416.56" y1="-43.18" x2="-416.56" y2="-48.26" width="0.1524" layer="91"/>
<junction x="-416.56" y="-43.18"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="-416.56" y1="-48.26" x2="-416.56" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-416.56" y1="-53.34" x2="-416.56" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-416.56" y="-53.34"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="-416.56" y1="-63.5" x2="-416.56" y2="-73.66" width="0.1524" layer="91"/>
<junction x="-416.56" y="-63.5"/>
<wire x1="-416.56" y1="-48.26" x2="-426.72" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-426.72" y1="-48.26" x2="-426.72" y2="-55.88" width="0.1524" layer="91"/>
<junction x="-416.56" y="-48.26"/>
<pinref part="U$21" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="CPOL3" gate="G$1" pin="-"/>
<wire x1="-304.8" y1="-86.36" x2="-304.8" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="-304.8" y1="-91.44" x2="-289.56" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="-289.56" y1="-86.36" x2="-289.56" y2="-91.44" width="0.1524" layer="91"/>
<junction x="-289.56" y="-91.44"/>
<wire x1="-289.56" y1="-91.44" x2="-274.32" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="-274.32" y1="-91.44" x2="-274.32" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="U$22" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="-312.42" y1="-60.96" x2="-312.42" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-325.12" y1="-66.04" x2="-325.12" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="STPM1" gate="G$1" pin="GNDD@1"/>
<wire x1="-345.44" y1="-83.82" x2="-345.44" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-345.44" y1="-66.04" x2="-342.9" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="STPM1" gate="G$1" pin="GNDD@0"/>
<wire x1="-342.9" y1="-66.04" x2="-325.12" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-325.12" y1="-66.04" x2="-312.42" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="-83.82" x2="-342.9" y2="-66.04" width="0.1524" layer="91"/>
<junction x="-342.9" y="-66.04"/>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="-325.12" y1="-66.04" x2="-297.18" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-297.18" y1="-66.04" x2="-297.18" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-325.12" y="-66.04"/>
<pinref part="U$23" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="3"/>
<wire x1="-355.6" y1="-17.78" x2="-355.6" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="-10.16" x2="-365.76" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="-10.16" x2="-365.76" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U$24" gate="G$1" pin="AGND"/>
</segment>
</net>
<net name="GND-ISO" class="0">
<segment>
<pinref part="SJ1" gate="G$1" pin="2"/>
<pinref part="GND-ISO1" gate="G$1" pin="GND-ISO"/>
<wire x1="-434.34" y1="-160.02" x2="-431.8" y2="-160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-391.16" y1="-185.42" x2="-408.94" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-408.94" y1="-185.42" x2="-408.94" y2="-177.8" width="0.1524" layer="91"/>
<pinref part="FB5" gate="G$1" pin="P$2"/>
<pinref part="GND-ISO2" gate="G$1" pin="GND-ISO"/>
<junction x="-408.94" y="-185.42"/>
<pinref part="SC101" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-309.88" y1="-185.42" x2="-299.72" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-299.72" y1="-185.42" x2="-299.72" y2="-177.8" width="0.1524" layer="91"/>
<pinref part="FB1" gate="G$1" pin="P$1"/>
<pinref part="GND-ISO3" gate="G$1" pin="GND-ISO"/>
<junction x="-299.72" y="-185.42"/>
<pinref part="SC100" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="-373.38" y1="-144.78" x2="-373.38" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-152.4" x2="-368.3" y2="-152.4" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="-368.3" y1="-152.4" x2="-363.22" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-363.22" y1="-152.4" x2="-363.22" y2="-147.32" width="0.1524" layer="91"/>
<wire x1="-368.3" y1="-152.4" x2="-368.3" y2="-154.94" width="0.1524" layer="91"/>
<junction x="-368.3" y="-152.4"/>
<pinref part="STPM1" gate="G$1" pin="VIN1"/>
<wire x1="-358.14" y1="-124.46" x2="-358.14" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-358.14" y1="-152.4" x2="-363.22" y2="-152.4" width="0.1524" layer="91"/>
<junction x="-363.22" y="-152.4"/>
<pinref part="GND-ISO4" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="-330.2" y1="-144.78" x2="-330.2" y2="-152.4" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-330.2" y1="-152.4" x2="-340.36" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="-152.4" x2="-340.36" y2="-147.32" width="0.1524" layer="91"/>
<pinref part="STPM1" gate="G$1" pin="VIN2"/>
<wire x1="-345.44" y1="-124.46" x2="-345.44" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-345.44" y1="-152.4" x2="-340.36" y2="-152.4" width="0.1524" layer="91"/>
<junction x="-340.36" y="-152.4"/>
<pinref part="GND-ISO5" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="STPM1" gate="G$1" pin="GNDA"/>
<wire x1="-322.58" y1="-106.68" x2="-317.5" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="-106.68" x2="-317.5" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="STPM1" gate="G$1" pin="GND_REF"/>
<wire x1="-317.5" y1="-111.76" x2="-317.5" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="-322.58" y1="-111.76" x2="-317.5" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-317.5" y="-111.76"/>
<pinref part="STPM1" gate="G$1" pin="GND_REG"/>
<wire x1="-322.58" y1="-101.6" x2="-317.5" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="-101.6" x2="-317.5" y2="-106.68" width="0.1524" layer="91"/>
<junction x="-317.5" y="-106.68"/>
<pinref part="GND-ISO6" gate="G$1" pin="GND-ISO"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="-307.34" y1="-124.46" x2="-307.34" y2="-127" width="0.1524" layer="91"/>
<wire x1="-307.34" y1="-127" x2="-297.18" y2="-127" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-297.18" y1="-127" x2="-297.18" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="-297.18" y1="-127" x2="-292.1" y2="-127" width="0.1524" layer="91"/>
<wire x1="-292.1" y1="-127" x2="-292.1" y2="-116.84" width="0.1524" layer="91"/>
<junction x="-297.18" y="-127"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="-292.1" y1="-127" x2="-284.48" y2="-127" width="0.1524" layer="91"/>
<wire x1="-284.48" y1="-127" x2="-284.48" y2="-119.38" width="0.1524" layer="91"/>
<junction x="-292.1" y="-127"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="-284.48" y1="-127" x2="-279.4" y2="-127" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="-127" x2="-279.4" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-284.48" y="-127"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="-279.4" y1="-127" x2="-271.78" y2="-127" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="-127" x2="-271.78" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-279.4" y="-127"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="-261.62" y1="-111.76" x2="-261.62" y2="-127" width="0.1524" layer="91"/>
<wire x1="-261.62" y1="-127" x2="-271.78" y2="-127" width="0.1524" layer="91"/>
<junction x="-271.78" y="-127"/>
<pinref part="GND-ISO7" gate="G$1" pin="GND-ISO"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="-347.98" y1="-60.96" x2="-332.74" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-332.74" y1="-60.96" x2="-332.74" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="-332.74" y1="-53.34" x2="-312.42" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="-53.34" x2="-325.12" y2="-53.34" width="0.1524" layer="91"/>
<junction x="-312.42" y="-53.34"/>
<pinref part="STPM1" gate="G$1" pin="VDDD"/>
<wire x1="-347.98" y1="-83.82" x2="-347.98" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="-325.12" y1="-53.34" x2="-297.18" y2="-53.34" width="0.1524" layer="91"/>
<junction x="-325.12" y="-53.34"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="-480.06" y1="-127" x2="-480.06" y2="-124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="-472.44" y1="-127" x2="-472.44" y2="-124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="-132.08" y1="-43.18" x2="-129.54" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="3"/>
<wire x1="-129.54" y1="-43.18" x2="-124.46" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-129.54" y="-43.18"/>
<pinref part="U$7" gate="G$1" pin="4"/>
<wire x1="-129.54" y1="-43.18" x2="-129.54" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BLUE" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="BLUE"/>
<wire x1="-144.78" y1="-12.7" x2="-154.94" y2="-12.7" width="0.1524" layer="91"/>
<label x="-154.94" y="-12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PC2(ADC2)"/>
<wire x1="-175.26" y1="-40.64" x2="-162.56" y2="-40.64" width="0.1524" layer="91"/>
<label x="-172.72" y="-40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="GREEN" class="0">
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PC1(ADC1)"/>
<wire x1="-175.26" y1="-38.1" x2="-162.56" y2="-38.1" width="0.1524" layer="91"/>
<label x="-172.72" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GREEN"/>
<wire x1="-144.78" y1="-17.78" x2="-154.94" y2="-17.78" width="0.1524" layer="91"/>
<label x="-154.94" y="-17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="RED" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RED"/>
<wire x1="-144.78" y1="-22.86" x2="-154.94" y2="-22.86" width="0.1524" layer="91"/>
<label x="-154.94" y="-22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328." gate="G$1" pin="PC0(ADC0)"/>
<wire x1="-175.26" y1="-35.56" x2="-162.56" y2="-35.56" width="0.1524" layer="91"/>
<label x="-172.72" y="-35.56" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
